// import React from 'react';
// import guide_img1 from  './../static/images/guide/guide_img1.png';
// import guide_img2 from './../static/images/guide/guide_img2.png';
// import guide_img3 from './../static/images/guide/guide_img3.png';
// import guide_img4 from './../static/images/guide/guide_img4.png';
// import guide_img5 from './../static/images/guide/guide_img5.png';

// import sidebar_btn from './../static/images/navigation/sidebar/sidebar_btn.png';
// import sidebar_close from './../static/images/navigation/sidebar/sidebar_close.png';

// //detail 1
// import h1 from './../static/images/hero/detail1/h1.png'
// import h2 from './../static/images/hero/detail1/h2.png'
// import h3 from './../static/images/hero/detail1/h3.png'
// import arrow from './../static/images/hero/detail1/arrow.png'
// //detail 2
// import h4 from './../static/images/hero/detail2/h4.png'
// import arrow_top from './../static/images/hero/detail2/arrow_top.png'
// import arrow_bottom from './../static/images/hero/detail2/arrow_bottom.png'

// //detail3 
// import h5 from './../static/images/hero/detail3/h5.png'
// import arrow_3 from './../static/images/hero/detail3/arrow3.png'
// import arrow_4 from './../static/images/hero/detail3/arrow4.png'
// import arrow_5 from './../static/images/hero/detail3/arrow5.png'

// //popup
// import icon from './../static/images/hero/detail3/icon.png'
// import zoom from './../static/images/hero/zoom.png'


// import hero_overview from './../static/images/hero/overview/hero_overview.png'
// //type tank
// import tank_phase from './../static/images/hero/tank/phase.png'
// import tank_map from './../static/images/hero/tank/map.png'
// import tank_icon from './../static/images/hero/tank/tank_icon.png'
// import tank_pos from './../static/images/hero/tank/tank_pos.png'
// import tank from './../static/images/hero/tank/tank.png'

// //type fighter
// import fighter_phase from './../static/images/hero/fighter/phase.png'
// import fighter_map from './../static/images/hero/fighter/map.png'
// import fighter_icon from './../static/images/hero/fighter/fighter_icon.png'
// import fighter_pos from './../static/images/hero/fighter/fighter_pos.png'
// import fighter from './../static/images/hero/fighter/fighter.png'

// //type assassin
// import assassin_phase from './../static/images/hero/assassin/phase.png'
// import assassin_map from './../static/images/hero/assassin/map.png'
// import assassin_icon from './../static/images/hero/assassin/assassin_icon.png'
// import assassin_pos from './../static/images/hero/assassin/assassin_pos.png'
// import assassin from './../static/images/hero/assassin/assassin.png'

// //mage
// import mage_phase from './../static/images/hero/mage/phase.png'
// import mage_map from './../static/images/hero/mage/map.png'
// import mage_icon from './../static/images/hero/mage/mage_icon.png'
// import mage_pos from './../static/images/hero/mage/mage_pos.png'
// import mage from './../static/images/hero/mage/mage.png'

// //carry
// import carry_phase from './../static/images/hero/carry/phase.png'
// import carry_map from './../static/images/hero/carry/map.png'
// import carry_icon from './../static/images/hero/carry/carry_icon.png'
// import carry_pos from './../static/images/hero/carry/carry_pos.png'
// import carry from './../static/images/hero/carry/carry.png'

// //support
// import support_phase from './../static/images/hero/support/phase.png'
// import support_map from './../static/images/hero/support/map.png'
// import support_icon from './../static/images/hero/support/support_icon.png'
// import support_pos from './../static/images/hero/support/support_pos.png'
// import support from './../static/images/hero/support/support.png'

// //skill
// import heal from './../static/images/hero/challenger_skill/heal.png'
// import flicker from './../static/images/hero/challenger_skill/flicker.png'
// import execute from './../static/images/hero/challenger_skill/execute.png'
// import punish from './../static/images/hero/challenger_skill/punish.png'
// import purify from './../static/images/hero/challenger_skill/purify.png'
// import roar from './../static/images/hero/challenger_skill/roar.png'


// //map laning
// import map from './../static/images/map/laning/map.png'
// import top_lane from './../static/images/map/laning/top_lane.png'
// import mid_lane from './../static/images/map/laning/mid_lane.png'
// import bot_lane from './../static/images/map/laning/bot_lane.png'
// import dot from './../static/images/map/laning/dot.png'
// import top_map from './../static/images/map/laning/top_map.png'
// import top_icon from './../static/images/map/laning/top_icon.png'
// import mid_icon from './../static/images/map/laning/mid_icon.png'
// import mid_map from './../static/images/map/laning/mid_map.png'
// import bot_icon from './../static/images/map/laning/bot_icon.png'
// import bot_map from './../static/images/map/laning/bot_map.png'

// //monster
// import monster_punish from './../static/images/map/monster/punish.png'
// import mon_map from './../static/images/map/monster/map.png'
// import dark_slayer from './../static/images/map/monster/dark_slayer.png'
// import dark_slayer_icon from './../static/images/map/monster/dark_slayer_icon.png'
// import sentry_hawk from './../static/images/map/monster/sentry_hawk.png'
// import sentry_hawk_icon from './../static/images/map/monster/sentry_hawk_icon.png'
// import spirit_sentinel from './../static/images/map/monster/spirit_sentinel.png'
// import spirit_sentinel_icon from './../static/images/map/monster/spirit_sentinel_icon.png'
// import jungle from './../static/images/map/monster/jungle.png'
// import jungle_icon from './../static/images/map/monster/jungle_icon.png'
// import red from './../static/images/map/monster/red.png'
// import red_icon from './../static/images/map/monster/red_icon.png'
// import blue from './../static/images/map/monster/blue.png'
// import blue_icon from './../static/images/map/monster/blue_icon.png'
// import abyssal_dragon from './../static/images/map/monster/abyssal_dragon.png'
// import abyssal_dragon_icon from './../static/images/map/monster/abyssal_dragon_icon.png'

// //map base
// import base_icon from './../static/images/map/base/base_icon.png'
// import base from './../static/images/map/base/base.png'
// import tower_icon from './../static/images/map/base/tower_icon.png'
// import outer_tower from './../static/images/map/base/outer_tower.png'
// import mid_tower  from './../static/images/map/base/mid_tower.png'
// import inner_tower from './../static/images/map/base/inner_tower.png'

// import e_base_icon from './../static/images/map/base/e_base_icon.png'
// import e_base from './../static/images/map/base/e_base.png'
// import e_tower_icon from './../static/images/map/base/e_tower_icon.png'
// import e_outer_tower from './../static/images/map/base/e_outer_tower.png'
// import e_mid_tower  from './../static/images/map/base/e_mid_tower.png'
// import e_inner_tower from './../static/images/map/base/e_inner_tower.png'
// import base_new_content from './../static/images/map/base/base_new_content.png'
// import enlarge_content from './../static/images/map/base/enlarge_content.png'

// import exclamation_icon from './../static/images/map/bush/exclamation_icon.png'
// import bush_map from './../static/images/map/bush/map.png'
// import bush from './../static/images/map/bush/bush.png'

// import item_box from './../static/images/item/item/item_box.png'
// import damage from './../static/images/item/itemtype/damage.png'
// import magic_damage from './../static/images/item/itemtype/magic_damage.png'
// import defense from './../static/images/item/itemtype/defense.png'
// import movement from './../static/images/item/itemtype/movement.png'
// import jungle_item from './../static/images/item/itemtype/jungle.png'
// import support_item from './../static/images/item/itemtype/support.png'

// import purchase from './../static/images/item/purchaseitem/purchase.png'
// import purchase2 from './../static/images/item/purchaseitem/purchase2.png'
// import itemset from './../static/images/item/purchaseitem/itemset.png'
// import modal_purchase from './../static/images/item/purchaseitem/modal_purchase.png'

// import modal_howto from './../static/images/item/howto/modal_howto.png'
// import howto from './../static/images/item/howto/howto.png'
// import howto2 from './../static/images/item/howto/howto2.png'

// import tips_box from './../static/images/item/tips/tips_box.png'
// import tips1 from './../static/images/item/tips/tips1.png'
// import tips2 from './../static/images/item/tips/tips2.png'
import overview_img from './../static/images/summary/overview_img.png'
import overview_boticon from './../static/images/summary/overview_boticon.png'

import homeui_img from './../static/images/summary/homeui_img.png'
import setting_1 from './../static/images/summary/setting_1.png'
import setting_2 from './../static/images/summary/setting_2.png'
import controlui_img from './../static/images/summary/controlui_img.png'

import control_1 from './../static/images/summary/control_1.png'
import control_2 from './../static/images/summary/control_2.png'
import control_3 from './../static/images/summary/control_3.png'
import control_4 from './../static/images/summary/control_4.png'
import control_5 from './../static/images/summary/control_5.png'
import control_6 from './../static/images/summary/control_6.png'
import control_7 from './../static/images/summary/control_7.png'
import control_8 from './../static/images/summary/control_8.png'
import control_9 from './../static/images/summary/control_9.png'
import control_10 from './../static/images/summary/control_10.png'

const guide_img1 =  'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/guide/guide_img1.png';
const guide_img2 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/guide/guide_img2.png';
const guide_img3 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/guide/guide_img3.png';
const guide_img4 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/guide/guide_img4.png';
const guide_img5 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/guide/guide_img5.png';

const sidebar_btn = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/navigation/sidebar/sidebar_btn.png';
const sidebar_close = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/navigation/sidebar/sidebar_close.png';

//detail 1
const h1 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail1/h1.png'
const h2 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail1/h2.png'
const h3 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail1/h3.png'
const arrow = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail1/arrow.png'
//detail 2
const h4 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail2/h4.png'
const arrow_top = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail2/arrow_top.png'
const arrow_bottom = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail2/arrow_bottom.png'

//detail3 
const h5 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail3/h5.png'
const arrow_3 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail3/arrow3.png'
const arrow_4 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail3/arrow4.png'
const arrow_5 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail3/arrow5.png'

//popup
const icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/detail3/icon.png'
const zoom = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/zoom.png'


const hero_overview = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/overview/hero_overview.png'
//type tank
const tank_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/tank/phase.png'
const tank_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/tank/map.png'
const tank_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/tank/tank_icon.png'
const tank_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/tank/tank_pos.png'
const tank = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/tank/tank.png'

//type fighter
const fighter_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/fighter/phase.png'
const fighter_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/fighter/map.png'
const fighter_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/fighter/fighter_icon.png'
const fighter_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/fighter/fighter_pos.png'
const fighter = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/fighter/fighter.png'

//type assassin
const assassin_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/assassin/phase.png'
const assassin_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/assassin/map.png'
const assassin_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/assassin/assassin_icon.png'
const assassin_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/assassin/assassin_pos.png'
const assassin = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/assassin/assassin.png'

//mage
const mage_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/mage/phase.png'
const mage_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/mage/map.png'
const mage_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/mage/mage_icon.png'
const mage_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/mage/mage_pos.png'
const mage = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/mage/mage.png'

//carry
const carry_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/carry/phase.png'
const carry_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/carry/map.png'
const carry_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/carry/carry_icon.png'
const carry_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/carry/carry_pos.png'
const carry = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/carry/carry.png'

//support
const support_phase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/support/phase.png'
const support_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/support/map.png'
const support_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/support/support_icon.png'
const support_pos = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/support/support_pos.png'
const support = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/support/support.png'

//skill
const heal = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/heal.png'
const flicker = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/flicker.png'
const execute = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/execute.png'
const punish = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/punish.png'
const purify = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/purify.png'
const roar = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/hero/challenger_skill/roar.png'


//map laning
const map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/map.png'
const top_lane = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/top_lane.png'
const mid_lane = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/mid_lane.png'
const bot_lane = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/bot_lane.png'
const dot = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/dot.png'
const top_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/top_map.png'
const top_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/top_icon.png'
const mid_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/mid_icon.png'
const mid_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/mid_map.png'
const bot_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/bot_icon.png'
const bot_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/laning/bot_map.png'

//monster
const monster_punish = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/punish.png'
const mon_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/map.png'
const dark_slayer = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/dark_slayer.png'
const dark_slayer_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/dark_slayer_icon.png'
const sentry_hawk = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/sentry_hawk.png'
const sentry_hawk_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/sentry_hawk_icon.png'
const spirit_sentinel = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/spirit_sentinel.png'
const spirit_sentinel_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/spirit_sentinel_icon.png'
const jungle = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/jungle.png'
const jungle_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/jungle_icon.png'
const red = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/red.png'
const red_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/red_icon.png'
const blue = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/blue.png'
const blue_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/blue_icon.png'
const abyssal_dragon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/abyssal_dragon.png'
const abyssal_dragon_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/monster/abyssal_dragon_icon.png'

//map base
const base_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/base_icon.png'
const base = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/base.png'
const tower_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/tower_icon.png'
const outer_tower = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/outer_tower.png'
const mid_tower  = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/mid_tower.png'
const inner_tower = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/inner_tower.png'

const e_base_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_base_icon.png'
const e_base = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_base.png'
const e_tower_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_tower_icon.png'
const e_outer_tower = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_outer_tower.png'
const e_mid_tower  = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_mid_tower.png'
const e_inner_tower = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/e_inner_tower.png'
const base_new_content = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/base_new_content.png'
const enlarge_content = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/base/enlarge_content.png'

const exclamation_icon = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/bush/exclamation_icon.png'
const bush_map = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/bush/map.png'
const bush = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/map/bush/bush.png'

const item_box = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/item/item_box.png'
const damage = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/damage.png'
const magic_damage = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/magic_damage.png'
const defense = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/defense.png'
const movement = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/movement.png'
const jungle_item = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/jungle.png'
const support_item = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/itemtype/support.png'

const purchase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/purchaseitem/purchase.png'
const purchase2 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/purchaseitem/purchase2.png'
const itemset = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/purchaseitem/itemset.png'
const modal_purchase = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/purchaseitem/modal_purchase.png'

const modal_howto = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/howto/modal_howto.png'
const howto = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/howto/howto.png'
const howto2 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/howto/howto2.png'

const tips_box = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/tips/tips_box.png'
const tips1 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/tips/tips1.png'
const tips2 = 'https://cdngarenanow-a.akamaihd.net/webth/rov/gameguide/item/tips/tips2.png'

export const Imglist = {
	guide_img1: guide_img1,
	guide_img2: guide_img2,
	guide_img3: guide_img3,
	guide_img4: guide_img4,
	guide_img5: guide_img5,
	sidebar_btn: sidebar_btn,
	sidebar_close: sidebar_close,
	
	h1:h1,
	h2:h2,
	h3:h3,
	arrow:arrow,

	h4:h4,
	arrow_top:arrow_top,
	arrow_bottom:arrow_bottom,
	
	h5:h5,
	arrow_3:arrow_3,
	arrow_4:arrow_4,
	arrow_5:arrow_5,

	icon:icon,
	zoom:zoom,

	hero_overview:hero_overview,

	tank_phase:tank_phase,
	tank_map:tank_map,
	heal:heal,
	flicker:flicker,
	tank_icon:tank_icon,
	tank_pos:tank_pos,
	tank:tank,
    fighter_phase : fighter_phase,
    fighter_map : fighter_map,
    fighter_icon : fighter_icon,
    fighter_pos : fighter_pos,
    fighter : fighter,
    execute : execute,

	assassin_phase: assassin_phase,
	assassin_map: assassin_map,
	assassin_icon: assassin_icon,
	assassin_pos: assassin_pos,
	assassin: assassin,
	punish: punish,

	mage_phase: mage_phase,
	mage_map: mage_map,
	mage_icon : mage_icon ,
	mage_pos: mage_pos,
	mage: mage,
	purify:purify,

	carry_phase: carry_phase,
	carry_map: carry_map,
	carry_icon: carry_icon,
	carry_pos: carry_pos,
	carry: carry,
	roar:roar,

	support_phase: support_phase,
	support_map : support_map ,
	support_icon : support_icon ,
	support_pos : support_pos ,
	support : support ,
	map:map,
	top_lane:top_lane,
	mid_lane:mid_lane,
	bot_lane:bot_lane,
	dot:dot,
	top_map:top_map,
	top_icon:top_icon,
	mid_map:mid_map,
	mid_icon:mid_icon,
	bot_icon: bot_icon,
	bot_map: bot_map,
	
	monster_punish:monster_punish,
	mon_map: mon_map,
	dark_slayer: dark_slayer,
	dark_slayer_icon: dark_slayer_icon,
	sentry_hawk: sentry_hawk,
	sentry_hawk_icon: sentry_hawk_icon,
	spirit_sentinel: spirit_sentinel,
	spirit_sentinel_icon: spirit_sentinel_icon,
	jungle: jungle,
	jungle_icon: jungle_icon,
	red: red,
	red_icon: red_icon,
	blue: blue,
	blue_icon: blue_icon,
	abyssal_dragon: abyssal_dragon,
	abyssal_dragon_icon: abyssal_dragon_icon,

	base_icon: base_icon,
	base: base,
	tower_icon: tower_icon,
	outer_tower: outer_tower,
	mid_tower: mid_tower,
	inner_tower: inner_tower,
	e_base_icon: e_base_icon,
	e_base: e_base,
	e_tower_icon: e_tower_icon,
	e_outer_tower: e_outer_tower,
	e_mid_tower: e_mid_tower,
	e_inner_tower: e_inner_tower,
	base_new_content:base_new_content,
	enlarge_content:enlarge_content,

	exclamation_icon:exclamation_icon,
	bush_map:bush_map,
	bush:bush,
	//item
	item_box: item_box,
	damage: damage,
	magic_damage: magic_damage,
	defense: defense,
	movement: movement,
	jungle_item: jungle_item,
	support_item : support_item,

	//purchaseitem
	purchase: purchase,
	purchase2: purchase2,
	itemset : itemset,
	modal_purchase:modal_purchase,

	modal_howto : modal_howto,
	howto: howto,
	howto2: howto2,

	tips_box:tips_box,
	tips1:tips1,
	tips2:tips2,

	//summary
	//overview
	overview_img :  overview_img,
	overview_boticon :  overview_boticon,

	//homeui
	homeui_img: homeui_img,
	setting_1: setting_1,
	setting_2: setting_2,
	controlui_img: controlui_img,
	control_1 : control_1,
	control_2 : control_2,
	control_3 : control_3,
	control_4 : control_4,
	control_5 : control_5,
	control_6 : control_6,
	control_7 : control_7,
	control_8 : control_8,
	control_9 : control_9,
	control_10 : control_10,
}
