import React,{Fragment} from 'react';

import './../styles/guide.scss'
import Header from './../components/navigation/header/Header'

import Sidebar from './../components/navigation/sidebar/Sidebar'
import { HorizontalSlide } from './../components/navigation/horizontalslide/HorizontalSlide'
import GuideButton from './../components/navigation/button/Button'

import Item from './../components/item/Item'
import ItemType from './../components/item/ItemType'
import PurchaseItem1 from './../components/item/PurchaseItem1'
import PurchaseItem2 from './../components/item/PurchaseItem2'

import HowItem1 from './../components/item/HowItem1'
import HowItem2 from './../components/item/HowItem2'

import Tips1 from './../components/item/Tips1'
import Tips2 from './../components/item/Tips2'

class ItemContainer extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			showSideBar: true,
			currentPage: 1,
			linkTo: false,
			lastPage: 5,
			pagelist:
			{
				1: {
					title:'ไอเทมคืออะไร?', 
					compo:<Item/>,
					},
				2: {
					title:'ประเภทของไอเทม',
					compo:<ItemType/>
					},
				3: {
					title:'การซื้อไอเทม',
					compo:<HorizontalSlide key={'purchaseslide'}>
							<PurchaseItem1/>
							<PurchaseItem2/>
						 </HorizontalSlide>
					},
				4: {
					title:'วิธีการซื้อไอเทม',
					compo:<HorizontalSlide key={'howtoslide'}>
							<HowItem1/>
							<HowItem2/>
						 </HorizontalSlide>
					},
				5: {
					title:'Tips',
					compo:<HorizontalSlide key={'tipsslide'} tips={true}>
							<Tips1/>
							<Tips2/>
						 </HorizontalSlide>
					}
			},
			herolist:[
				{
					title:'ไอเทมคืออะไร?',
					page: 1,
					id:'item_item',
				},
				{
					title:'ประเภทของไอเทม',
					page: 2,
					id:'item_type',
				},
				{
					title:'การซื้อไอเทม',
					page: 3,
					id:'item_purchase',
				},
				{
					title:'วิธีการซื้อไอเทม',
					page: 4,
					id:'item_howto',
				},
				{
					title:'Tips',
					page: 5,
					id:'item_tips',
				},
			],
		}
	}

	sidebarToggleHandler = () =>{
		this.setState((prevState) => {
			return {showSideBar: !prevState.showSideBar};
		});
	}

	getPage = (page,title)=>{
		if( page === this.state.lastPage){
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:'/summary'
			})
		}else{
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:false
			})
		}
	}
	
	btnNext = ()=>{
		if(this.state.currentPage !== this.state.lastPage){
			this.setState((prevState, props) => ({
		    	currentPage: prevState.currentPage + 1,
		    	linkTo:false
			}));
		}else{
			this.setState({
				linkTo:'/summary'
			})
		}
		if(this.state.currentPage === this.state.lastPage-1){
			this.setState({
				linkTo:'/summary'
			})
		}
	}
	componentDidMount(){
		setTimeout(() => {
		  this.setState({
		  	showSideBar:false
		  })
		}, 1500)
	}

	componentDidUpdate(prevProps, prevState){
		if(this.state.currentPage !== prevState.currentPage){
				this.setState({
					showSideBar:true
				})
				clearTimeout(this.timeout);
				this.timeout = setTimeout(() => {
					this.setState({
						showSideBar: false
					})
				}, 1500)
		}
	}
	render() {
		return (
			<div className="guide">
				<Header mode='default' to='/'>
					<Fragment key={'title'}>
 				 		{this.state.pagelist[this.state.currentPage].title}
					 </Fragment>
				</Header>
				<Sidebar 
					show={this.state.showSideBar} onClick={()=>this.sidebarToggleHandler()}
					list={this.state.herolist}
					currentPage={this.state.currentPage}
					getPage= {this.getPage}
				/>
				<div className="guide__content middlealign">
					<Fragment>
 				 		{this.state.pagelist[this.state.currentPage].compo}
					</Fragment>
				</div>
				<GuideButton abs={true} onClick={this.btnNext} linkTo={this.state.linkTo} id={'itempage_btn'}>
					ถัดไป
				</GuideButton>
			</div>
		);
	}
}

export default ItemContainer;