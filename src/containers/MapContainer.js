import React,{Fragment} from 'react';

import './../styles/guide.scss'
import Header from './../components/navigation/header/Header'

import Sidebar from './../components/navigation/sidebar/Sidebar'
import GuideButton from './../components/navigation/button/Button'

import Laning from './../components/map/Laning'
import Monster from './../components/map/Monster'
import BaseTower from './../components/map/BaseTower'
import Bush from './../components/map/Bush'

class MapContainer extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			showSideBar: true,
			currentPage: 1,
			linkTo: false,
			lastPage: 4,
			pagelist:
			{
				1: {
					title:'การแบ่งเลน', 
					compo:<Laning/>,
					},
				2: {
					title:'มอนสเตอร์',
					compo:<Monster/>
					},
				3: {
					title:'ฐานและป้อม',
					compo:<BaseTower/>
					},
				4: {
					title:'ป่าและพุ่มไม้',
					compo:<Bush/>
					},
			},
			herolist:[
				{
					title:'การแบ่งเลน',
					page: 1,
					id:'map_lane',
				},
				{
					title:'มอนสเตอร์',
					page: 2,
					id:'map_monster',
				},
				{
					title:'ฐานและป้อม',
					page: 3,
					id:'map_basetower'
				},
				{
					title:'ป่าและพุ่มไม้',
					page: 4,
					id:'map_bush',
				},
			],
		}
	}

	sidebarToggleHandler = () =>{
		this.setState((prevState) => {
			return {showSideBar: !prevState.showSideBar};
		});
	}
	
	getPage = (page,title)=>{
		if( page === this.state.lastPage){
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:'/item'
			})
		}else{
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:false
			})
		}
	}
	
	btnNext = ()=>{
		if(this.state.currentPage !== this.state.lastPage){
			this.setState((prevState, props) => ({
		    	currentPage: prevState.currentPage + 1,
		    	linkTo:false
			}));
		}else{
			this.setState({
				linkTo:'/item'
			})
		}
		if(this.state.currentPage === this.state.lastPage-1){
			this.setState({
				linkTo:'/item'
			})
		}
	}
	componentDidMount(){
		setTimeout(() => {
		  this.setState({
		  	showSideBar:false
		  })
		}, 1500)
	}

	componentDidUpdate(prevProps, prevState){
		if(this.state.currentPage !== prevState.currentPage){
				this.setState({
					showSideBar:true
				})
				clearTimeout(this.timeout);
				this.timeout = setTimeout(() => {
					this.setState({
						showSideBar: false
					})
				}, 1500)
		}
	}
	render() {
		return (
			<div className="guide">
				<Header mode='default' to='/'>
					<Fragment key={'title'}>
 				 		{this.state.pagelist[this.state.currentPage].title}
					 </Fragment>
				</Header>
				<Sidebar 
					show={this.state.showSideBar} onClick={()=>this.sidebarToggleHandler()}
					list={this.state.herolist}
					currentPage={this.state.currentPage}
					getPage= {this.getPage}
				/>
				<div className="guide__content middlealign">
					<Fragment>
 				 		{this.state.pagelist[this.state.currentPage].compo}
					</Fragment>
				</div>
				<GuideButton abs={true} onClick={this.btnNext} linkTo={this.state.linkTo} id={'mappage_btn'}>
					ถัดไป
				</GuideButton>
			</div>
		);
	}
}

export default MapContainer;