import React from 'react';

import './../styles/guide.scss'
import Header from './../components/navigation/header/Header'
import MainGuidePage from './../components/guide/Guide'
import MainGuideFooter from './../components/guide/footer'
import GuideButton from './../components/navigation/button/Button'

class HomeContainer extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			linkTo: '/hero'
		}
	}
	getParam(_param) {
		var pStr =
		    window.location.hash.toString() || window.location.search.toString(),
		  r = new RegExp('[?&]*' + _param + '=([^&]+)'),
		  m = pStr.match(r);
		if (m) return m[1].replace('"', '');
		else return '';
	}

	getCookie(name) {
		var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
		if (match) return match[2];
	}

	delCookie(name) {
		document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	setCookie(name, value, days) {
		var expires = '';
		if (days) {
		  var date = new Date();
		  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
		  expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + (value || '') + expires + '; path=/';
	}
	checkLogin() {
    // var self = this;
    var oauthParam = this.getParam('access_token') || this.getParam('token');
    var oauthCookie = this.getCookie('oauth_token');
    var sTicket = this.getParam('sTicket');
    var sTicketCookie = this.getCookie('detect');

    if (oauthParam && oauthParam.length > 0) {
      this.setCookie('oauth_token', oauthParam, 1);
      if (typeof sTicket !== 'undefined') {
        if (sTicket !== '') {
          this.setCookie('detect', '532c28d5412dd75bf975fb951c740a30', 1);
        }
      }
      var url = window.location.href.substring(
        0,
        window.location.href.lastIndexOf('?')
      ); // get rid of the query string
      window.location.href = url; // we need hard refresh
    } else if (oauthCookie && oauthCookie.length > 0) {
      if (typeof sTicketCookie === 'undefined') {
        sTicketCookie = 'test';
      }
    } else {
      console.log('Not Log in');
    }
  }

  componentDidMount(){
  	this.checkLogin();
  }
	render() {
		return (
			<div className="guide">
				<Header mode='default' to='/'>
					Guide
				</Header>
				<div className="guide__content middlealign">
					<div>
						{/* <MainGuidePage imglist={[
							{'img':'guide_img1',id:'hero',title:'ฮีโร่',sub:'',link:'/hero'},
							{'img':'guide_img2',id:'map',title:'แผนที่',sub:'',link:'/map'},
							{'img':'guide_img3',id:'item',title:'ไอเทม',sub:'',link:'/item'},
							{'img':'guide_img4',id:'summary',title:'สรุป',sub:'',link:'/summary'},
							{'img':'guide_img5',id:'advance',title:'คู่มือระดับสูง',sub:'(Coming Soon)',link:'/advance'}]}
						/> */}
						<MainGuidePage imglist={[
							{'img':'guide_img1',id:'hero',title:'ฮีโร่',sub:'',link:'/hero'},
							{'img':'guide_img2',id:'map',title:'แผนที่',sub:'',link:'/map'},
							{'img':'guide_img3',id:'item',title:'ไอเทม',sub:'',link:'/item'},
							{'img':'guide_img4',id:'summary',title:'สรุป',sub:'',link:'/summary'}]}
						/>
						<MainGuideFooter/>	
						<GuideButton abs={true} linkTo={this.state.linkTo} id={'startguide'}>
							เริ่มเลย !
						</GuideButton>
					</div>
				</div>
			</div>
		);
	}
}

export default HomeContainer;