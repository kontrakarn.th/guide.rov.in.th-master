import React from 'react';
import {Fragment} from 'react';

import './../styles/guide.scss'
import Header from './../components/navigation/header/Header'
import MainGuidePage from './../components/guide/Guide'
import MainGuideFooter from './../components/guide/footer'

import GuideButton from './../components/navigation/button/Button'
import Sidebar from './../components/navigation/sidebar/Sidebar'

import Overview from './../components/summary/Overview'
import HomeUI from './../components/summary/HomeUI'
import Setting from './../components/summary/Setting'
import Controls from './../components/summary/Controls'
import Beginner from './../components/hero/Beginner'

class SummaryContainer extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			showSideBar: false,
			currentPage: 1,
			linkTo: false,
			lastPage: 5,
			pagelist:
			{
				1: {
					title:'ภาพรวมของเกม', 
					compo:<Overview/>,
					},
				2: {
					title:'หน้า Home',
					compo:<HomeUI/>
					},
				3: {
					title:'Settings',
					compo: <Setting />,
					},
				4: {
					title:'การควบคุมในเกม',
					compo:<Controls/>,
					},
				5: {
					title:'วิธีการเล่นเบื้องต้น',
					compo:<Beginner/>,
					},
			},
			herolist:[
				{
					title:'ภาพรวมของเกม',
					page: 1
				},
				{
					title:'หน้า Home',
					page: 2
				},
				{
					title:'Settings',
					page: 3
				},
				{
					title:'การควบคุมในเกม',
					page: 4
				},
				{
					title:'วิธีการเล่นเบื้องต้น',
					page:5
				}
			],
		}
	}

		sidebarToggleHandler = () =>{
		this.setState((prevState) => {
			return {showSideBar: !prevState.showSideBar};
		});
	}

	getPage = (page,title)=>{
		if( page === this.state.lastPage){
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:'/item'
			})
		}else{
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:false
			})
		}
	}
	
	btnNext = ()=>{
		if(this.state.currentPage !== this.state.lastPage){
			this.setState((prevState, props) => ({
		    	currentPage: prevState.currentPage + 1,
		    	linkTo:false
			}));
		}else{
			this.setState({
				linkTo:'/advance'
			})
		}
		if(this.state.currentPage === this.state.lastPage-1){
			this.setState({
				linkTo:'/advance'
			})
		}
	}
	render() {
		return (
			<div className="guide">
				<Header mode='default' to='/'>
					<Fragment key={'title'}>
 				 		{this.state.pagelist[this.state.currentPage].title}
					 </Fragment>
				</Header>
				<Sidebar 
					show={this.state.showSideBar} onClick={()=>this.sidebarToggleHandler()}
					list={this.state.herolist}
					currentPage={this.state.currentPage}
					getPage= {this.getPage}
				/>
				<div className="guide__inner">
					<Fragment>
 				 		{this.state.pagelist[this.state.currentPage].compo}
					</Fragment>
				</div>
				<GuideButton abs={true} onClick={this.btnNext} linkTo={this.state.linkTo}>
					ถัดไป
				</GuideButton>
				{/*<Header mode='default' to='/'>
					สรุป
				</Header>
				<div className="guide__content middlealign">
				  	<div className="advance">Coming Soon</div>
				</div>*/}
			</div>
		);
	}
}

export default SummaryContainer;