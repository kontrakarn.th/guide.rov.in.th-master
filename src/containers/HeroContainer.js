import React,{Fragment} from 'react';

import './../styles/guide.scss'
import Header from './../components/navigation/header/Header'

import Sidebar from './../components/navigation/sidebar/Sidebar'
import { HorizontalSlide } from './../components/navigation/horizontalslide/HorizontalSlide'

import Herodetail1 from './../components/hero/Herodetail1'
import Herodetail2 from './../components/hero/Herodetail2'
import Herodetail3 from './../components/hero/Herodetail3'

import Overview from './../components/hero/overview/Overview'
import Tank1 from './../components/hero/herotype/Tank1'
import Tank2 from './../components/hero/herotype/Tank2'

import Fighter1 from './../components/hero/herotype/Fighter1'
import Fighter2 from './../components/hero/herotype/Fighter2'

import Assassin1 from './../components/hero/herotype/Assassin1'
import Assassin2 from './../components/hero/herotype/Assassin2'

import Mage1 from './../components/hero/herotype/Mage1'
import Mage2 from './../components/hero/herotype/Mage2'

import Carry1 from './../components/hero/herotype/Carry1'
import Carry2 from './../components/hero/herotype/Carry2'

import Support1 from './../components/hero/herotype/Support1'
import Support2 from './../components/hero/herotype/Support2'

import Manner from './../components/hero/Manner'
import Beginner from './../components/hero/Beginner'
import GuideButton from './../components/navigation/button/Button'


class HeroContainer extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			showSideBar: true,
			currentPage: 1,
			linkTo: false,
			lastPage: 16,			
			pagelist:
			{
				1: {
					title:'มารยาทการเล่น',
					compo: <Manner/>
					},
				2: {
					title:'วิธีการเล่นเบื้องต้น',
					compo: <Beginner/>
					},
				3: {
					title:'เกี่ยวกับฮีโร่', 
					compo:<HorizontalSlide>
							<Herodetail1/>
							<Herodetail2/>
							<Herodetail3/>
						 </HorizontalSlide>,
					},
				4: {
					title:'ประเภทของฮีโร่',
					compo:<Overview/>
					},
				5: {
					title:'แทงค์ (Tank)',
					compo:<Tank1/>
					},
				6: {
					title:'แทงค์ (Tank)',
					compo:<Tank2/>
					},
				7: {
					title:'ไฟเตอร์ (Fighter)',
					compo:<Fighter1/>
					},
				8: {
					title:'ไฟเตอร์ (Fighter)',
					compo:<Fighter2/>
					},
				9: {
					title:'แอสซาซิน (Assassin)',
					compo:<Assassin1/>
					},
				10: {
					title:'แอสซาซิน (Assassin)',
					compo:<Assassin2/>
					},
				11: {
					title:'เมจ (Mage)',
					compo:<Mage1/>
					},
				12: {
					title:'เมจ (Mage)',
					compo:<Mage2/>
					},
				13: {
					title:'แครี่(Carry)',
					compo:<Carry1/>
					},
				14: {
					title:'แครี่(Carry)',
					compo:<Carry2/>
					},
				15: {
					title:'ซัพพอร์ท (Support)',
					compo:<Support1/>
					},
				16: {
					title:'ซัพพอร์ท (Support)',
					compo:<Support2/>
					},
			},
			herolist:[
				{
					title:'เกี่ยวกับฮีโร่',
					page: 3,
					id:'hero_detail',
				},
				{
					title:'ประเภทของฮีโร่',
					page: 4,
					id:'hero_type',
				},
				{
					title:'แทงค์ (Tank)',
					page: 5,
					page2: 6,
					id:'hero_tank',
				},
				{
					title:'ไฟเตอร์ (Fighter)',
					page: 7,
					page2: 8,
					id:'hero_fighter',
				},
				{
					title:'แอสซาซิน (Assassin)',
					page: 9,
					page2: 10,
					id:'hero_assassin',
				},
				{
					title:'เมจ (Mage)',
					page: 11,
					page2: 12,
					id:'hero_mage',
				},
				{
					title:'แครี่(Carry)',
					page: 13,
					page2: 14,
					id:'hero_carry',
				},
				{
					title:'ซัพพอร์ท (Support)',
					page: 15,
					page2: 16,
					id:'hero_support',
				}
			],
		}
	}

	sidebarToggleHandler = () =>{
		this.setState((prevState) => {
			return {showSideBar: !prevState.showSideBar};
		});
	}

	getPage = (page,title)=>{
		if( page === this.state.lastPage){
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:'/map'
			})
		}else{
			this.setState({
				currentPage: page,
				showSideBar: false,
				linkTo:false
			})
		}
	}
	
	btnNext = ()=>{
		if(this.state.currentPage !== this.state.lastPage){
			this.setState((prevState, props) => ({
		    	currentPage: prevState.currentPage + 1,
		    	linkTo:false
			}));
		}else{
			this.setState({
				linkTo:'/map'
			})
		}
		if(this.state.currentPage === this.state.lastPage-1){
			this.setState({
				linkTo:'/map'
			})
		}
	}

	componentDidUpdate(prevProps, prevState){
		if(this.state.currentPage !== prevState.currentPage){
			// if(this.state.currentPage === 2 && this.state.checksidebar === false){
			// 	this.setState({checksidebar: true})
			// 	setTimeout(() => {
				
			// 	}, 2000)
			// }
			if(this.state.currentPage > 2){
				this.setState({
					showSideBar:true
				})
				clearTimeout(this.timeout);
				this.timeout = setTimeout(() => {
					this.setState({
						showSideBar: false
					})
				}, 1500)
			}
		}
	}
	render() {
		return (
			<div className="guide">
				<Header mode='default' to='/'>
					<Fragment key={'title'}>
 				 		{this.state.pagelist[this.state.currentPage].title}
					 </Fragment>
				</Header>
				{
					this.state.currentPage > 2 &&
						<Fragment>
						<Sidebar 
							show={this.state.showSideBar} onClick={()=>this.sidebarToggleHandler()}
							list={this.state.herolist}
							currentPage={this.state.currentPage}
							getPage= {this.getPage}
						/>
						</Fragment>
				}
				<div className="guide__content middlealign">
					<Fragment>
						{this.state.pagelist[this.state.currentPage].compo}
					</Fragment>
				</div>
				<GuideButton abs={true} onClick={this.btnNext} linkTo={this.state.linkTo} id={'heropage_btn'}>
					ถัดไป
				</GuideButton>
			</div>
		);
	}
}

export default HeroContainer;