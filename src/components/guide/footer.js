import React from 'react';
import styles from './footer.module.scss';

export default props=>{
	return(
		<div className={styles['footer']}>
			<div className={styles['footer__textblog']}>
				คำแนะนำ<span className={styles['text--red']}>สำหรับผู้เล่นที่ต้องการเข้าใจเกม</span> และสนุกกับการเล่นเกมมากขึ้น
			</div>
		</div>
	);
}