import React from 'react';
import { Link } from 'react-router-dom';
import styles from './../guide/guide.module.scss'
import {Imglist} from './../../constants/import_image';

export default props=>{
	const { className="guide",  imglist} = props;
	return(
		<div className={styles[className]}>
			{
				imglist.map((img,key)=>{
					return(
						<Link className={styles['guide__content']} id={img.id} key={'guide'+ key} to={img.link}>
							<img className={styles['guide__img']} src={Imglist[img.img]} alt=''/>
							<div className={styles['guide__title']}>
								<div className={styles['guide__title--main']}>{img.title}</div>
								<div className={styles['guide__title--sub']}>{img.sub}</div>
							</div>
						</Link>
					);
				})
			}
		</div>
	)
}