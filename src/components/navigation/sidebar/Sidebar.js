import React from 'react';
import styles from './sidebar.module.scss'
import {Imglist} from './../../../constants/import_image';

export default props=>{
	let sidebarClasses = [styles['sidebar'], styles.close]
	if (props.show){
		sidebarClasses = [styles['sidebar'], styles.open]
	}
	return(
			<div className={sidebarClasses.join(' ')}>
					<div className={styles['sidebar__btn']} onClick={props.onClick}>
						<img src={props.show ? Imglist['sidebar_close'] : Imglist['sidebar_btn']} alt=""/>
					</div>
					<ul className={styles['itemlist']}>
						{
							props.list.map((each,index)=>{
								return(
									<li key={'title'+ index} 
										className={props.currentPage === each.page || props.currentPage === each.page2 ? styles['active'] : ''}
										onClick={()=>props.getPage(each.page,each.title)}
										id={props.id}
										>
										 {each.title}
									</li>
								);
							})
						}
					</ul>
			</div>
	);

}