import React from 'react';
import styles from './popupbtn.module.scss'
// import {Imglist} from './../../../constants/import_image.js'
import Modal from 'react-responsive-modal';

export default class Popupbtn extends React.Component{
	constructor(props){
		super(props);
		this.state ={
			open: false,
		}
	}
	
	 onOpenModal = () => {
    	this.setState({ open: true });
	  };

	  onCloseModal = () => {
	    this.setState({ open: false });
	  };

	render() {
		// console.log(this.props.cusclass)
		return (
			<div className={ this.props.cusclass && this.props.cusclass ? styles[this.props.cusclass] : styles['popup']} 
				style={this.props.instyle}
			>
				<img style={this.props.imgstyle} src={this.props.img} alt="" onClick={this.onOpenModal} id={this.props.id}/>
				<Modal 
					classNames={{ overlay: styles['modal__overlay'] , modal: styles['modal__wrapper'] , closeButton: styles['modal__closebtn']}} 
					open={this.state.open} 
					onClose={this.onCloseModal} 
					center>
		          	{/*<ShowMessage message={this.props.message} />*/}
		          	{this.props.children && this.props.children}
		        </Modal>
			</div>
		);
	}
}

// function ShowMessage(props){
// 	const message = props.message;
// 	return(
// 		message.map((item,key)=>{
// 			return(
// 				<Fragment>
// 					<span className={styles['text--blue']}>
// 						{item.title}
// 					</span>
// 					<span> = {item.content}</span><br/>
// 				</Fragment>
// 			)
// 		})
// 	)
// }