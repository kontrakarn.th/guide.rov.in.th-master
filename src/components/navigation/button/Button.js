import React,{Fragment} from 'react';
import { Link } from 'react-router-dom';
import styles from './button.module.scss'

export default props=>{
	const { className = 'button', children} = props;
	return(
		<Fragment>
			{
				props.linkTo
				?
					<Link className={styles[className]} onClick={props.onClick} 
						to={props.linkTo}
						id={props.id}
						style={{	
							position: props.abs? 'absolute' : 'unset'
						}}
					>
						{children}
					</Link>
				:
					<span className={styles[className]} onClick={props.onClick}			
						id={props.id}
						style={{	
							position: props.abs? 'absolute' : 'unset'
						}}
					>
						{children}
					</span>
			}
		</Fragment>
	);
}