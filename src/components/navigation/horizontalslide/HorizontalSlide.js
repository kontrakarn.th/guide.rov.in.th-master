import React from 'react';
import Slider from "react-slick";
import './horizontalSlide.css'
export class HorizontalSlide extends React.Component {
    constructor(props){
        super(props);
        this.swiper = null;
        this.state = {

        }
    }
    
    render() {
        const settings = {
          dots: true,
          infinite: false,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
          nextArrow:<HideSlide/>,
          prevArrow:<HideSlide/>,
          appendDots: dots => (
            <div
              style={{bottom:-10}}
            >
              <ul style={{ margin: "0px" }}> {dots} </ul>
            </div>
          ),
        };
        const normal = {
                        height:'100%',
                        width:'100%',
                        maxHeight:'calc(100vw / 16 * 9)',
                        maxWidth:'calc(100vh / 9 * 16)',
                      };
        const tipsstyle = {height: 'calc(100vh)',width:'100vw'};
        return (
            <div style={this.props.tips === true ? {...tipsstyle} : {...normal}} key={this.props.key}>
                 <Slider {...settings}>
                    {this.props.children.length > 0
                      ?
                      this.props.children.map((item,index)=>{
                        return (
                            <div key={index}>
                                {item}
                            </div>
                        )
                      })
                      :
                        this.props.children
                    }
                </Slider>
            </div>
        )
    }
}

function HideSlide(props) {
    const {style, onClick} = props;
    return (
        <div
        className='disarrow'
        style={{ ...style,}}
        onClick={onClick}
        >
            
        </div>
    );
}
