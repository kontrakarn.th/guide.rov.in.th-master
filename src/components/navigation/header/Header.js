import React from 'react';
import styles from './header.module.scss';
import { Link } from 'react-router-dom';
import backbtn from './../../../static/images/navigation/back_btn.png'

export default props => {
	const { mode = "default", children , ...rest } = props;
	return(
		<div className={styles[mode]}>
			<Link {...rest}>
				<img src={backbtn} onClick={props.onCLick} alt=""/>
			</Link>
			<span>
			{
				props.headtitle
				?
					props.headtitle
				:
					children
			}
			</span>
		</div>
	);
}
