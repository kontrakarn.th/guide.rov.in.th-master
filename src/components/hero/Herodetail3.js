import React,{Fragment} from 'react';
import styles from './hero_detail.module.scss'
import {Imglist} from './../../constants/import_image';
import Popupbtn from './../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ออบเจกต์',
		'content':'ครีป มอนสเตอร์ ฮีโร่ และป้อม'
	},
	{
		'title': 'สกิล',
		'content':'ความสามารถพิเศษเฉพาะของแต่ละตัวละคร โดยฮีโร่แต่ละตัวจะมี 4 สกิล'
	},
	{	
		'title': 'ครีป',
		'content':'ตัวละครที่ถูกปล่อยออกมาจากทั้งฝั่งเรา และฝั่งศัตรู ครีปทั้ง 2 ฝั่งมีความสามารถเหมือนกัน และครีปจะเดินมาต่อสู้กันโดยอัตโนมัติ เมื่อฮีโร่ฆ่าครีปของฝั่งศัตรูจะได้รับเงิน และค่าประสบการณ์'
	},
	{
		'title': 'มอนสเตอร์',
		'content':'สัตว์ที่อยู่ในป่าของเกม เมื่อเราสามารถฆ่ามอนสเตอร์ได้ จะได้รับเงิน ค่าประสบการณ์และพลังที่ช่วยเพิ่มความสามารถได้ในระยะเวลาหนึ่ง (บัฟ)'
	},
]
export default props=>{
	return(
		<div className={styles['detail']}>
			<div className={styles['detail__container']}>
				 <div className={styles['detail__content']}>
				 	การโจมตีของฮีโร่ มี 2 แบบ ได้แก่  
				 	<span className={styles['text--red']}> โจมตีธรรมดา </span>
				 	และ 
				 	<span className={styles['text--red']}>โจมตีโดยใช้สกิล </span>
				 	ซ่ึงฮีโร่แต่ละตัวจะมีการโจมตี และสร้างความเสียหาย (ดาเมจ) ให้แก่ศัตรูในรูปแบบที่แตกต่างกัน ดังนี้
				 </div>
				 <div className={styles['detail3__contentwrap']}>
				 	<div className={styles['detail3__imgbox']}>
				 		<img  src={Imglist['h5']} alt=""/>
				 	</div>
				 	{/*<div className={styles['detail3__arrow--top'] + ' ' + styles['detail3__arrow']}>
				 		<img  src={Imglist['arrow_3']} alt=""/>
				 	</div>
				 	<div className={styles['detail3__arrow--center'] + ' ' + styles['detail3__arrow']}>
				 		<img  src={Imglist['arrow_4']} alt=""/>
				 	</div>
				 	<div className={styles['detail3__arrow--bot'] + ' ' + styles['detail3__arrow']}>
				 		<img  src={Imglist['arrow_5']} alt=""/>
				 	</div>*/}
				 	{/*<div className={styles['detail3__textbox']+ ' ' + styles['detail3__textbox--1']}>
			 			<span className={styles['text--red']}>ดาเมจกายภาพ = สีแดง</span> <br/>
			 			• เกิดจากการโจมตีปกติ, สกิลกายภาพ และผลของไอเทมโจมตี<br/>
						• พลังโจมตีจะลดลง เมื่อศัตรูมีเกราะป้องกันกายภาพ
				 	</div>
				 	<div className={styles['detail3__textbox'] + ' ' + styles['detail3__textbox--2']}>
				 		<span className={styles['text--purple']}>ดาเมจเวท = สีม่วง</span> <br/>
				 		• เกิดจากการใช้สกิลเวท และผลของไอเทมเวท<br/>
						• พลังโจมตีจะลดลง เมื่อศัตรูมีเกราะป้องกันเวท
				 	</div>
				 	<div className={styles['detail3__textbox'] + ' ' + styles['detail3__textbox--3']}>
				 		<span>ทรูดาเมจ = สีขาว</span><br/>
				 		• เป็นความเสียหายจริง ที่เกิดจากการใช้สกิล<br/>
						• พลังโจมตีจะ <span className={styles['text--red']}>“ไม่” </span> ลดลงแม้ว่าศัตรูมีเกราะป้องกันใดๆ
				 	</div>*/}
				 	<div className={styles['detail3__textwrap']}>
				 		<div className={styles['detail3__textbox']+ ' ' + styles['detail3__textbox--1']}>
				 			<span className={styles['text--red']}>ดาเมจกายภาพ = สีแดง</span> <br/>
				 			• เกิดจากการโจมตีปกติ, สกิลกายภาพ และผลของไอเทมโจมตี<br/>
							• พลังโจมตีจะลดลง เมื่อศัตรูมีเกราะป้องกันกายภาพ
					 	</div>
					 	<div className={styles['detail3__textbox'] + ' ' + styles['detail3__textbox--2']}>
					 		<span className={styles['text--purple']}>ดาเมจเวท = สีม่วง</span> <br/>
					 		• เกิดจากการใช้สกิลเวท และผลของไอเทมเวท<br/>
							• พลังโจมตีจะลดลง เมื่อศัตรูมีเกราะป้องกันเวท
					 	</div>
					 	<div className={styles['detail3__textbox'] + ' ' + styles['detail3__textbox--3']}>
					 		<span>ทรูดาเมจ = สีขาว</span><br/>
					 		• เป็นความเสียหายจริง ที่เกิดจากการใช้สกิล<br/>
							• พลังโจมตีจะ <span className={styles['text--red']}>“ไม่” </span> ลดลงแม้ว่าศัตรูมีเกราะป้องกันใดๆ
					 	</div>
				 	</div>
				 </div>
				 {/*<Popupbtn
				 	message={[
				 		{'title': 'ออบเจกต์','content':'ครีป มอนสเตอร์ ฮีโร่และป้อม'},
				 		{'title': 'สกิล','content':'ความสามารถพิเศษเฉพาะของแต่ละตัวละคร โดยฮีโร่แต่ละตัวจะมี 4 สกิล'},
				 		{'title': 'ครีป','content':'ตัวละครที่ถูกปล่อยออกมาจากทั้งฝั่งเราและฝั่งศัตรู ครีปทั้ง 2 ฝั่งมีความสามารถเหมือนกัน และครีปจะเดินมาต่อสู้กันโดยอัตโนมัติ เมื่อฮีโร่ฆ่าครีปของฝั่งศัตรูจะได้รับเงินและค่าประสบการณ์'},
				 		{'title': 'มอนสเตอร์','content':'สัตว์ที่อยู่ในป่าของเกม เมื่อเราสามารถฆ่ามอนสเตอร์ได้ จะได้รับเงิน ค่าประสบการณ์และพลังที่ช่วยเพิ่มความสามารถได้ในระยะเวลาหนึ่ง (บัฟ)'},
				 	]}/>*/}
				 <Popupbtn img={Imglist['icon']} cusclass={'detail3'} id={'hero_detail3'}>
				 {
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				 }
				 </Popupbtn>
			 </div>
		</div>
	);
}