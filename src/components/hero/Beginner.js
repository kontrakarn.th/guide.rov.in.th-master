import React from 'react'
import styles from './hero_detail.module.scss'

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['beginner__container']}>
			 	<div className={styles['beginner__contentbox']}>
			 		<div className={styles['beginner__title']}>
			 			ทำลายป้อมศัตรู ​= ชนะ
			 		</div>
			 		<ul>
			 			<li>
			 				<span className={styles['text--red']}>รู้หน้าที่ </span>
			 				สกิล ตำแหน่งการยืนของตัวเอง
			 			</li>
			 			<li>
			 				<span className={styles['text--red']}>มองมินิแมพบ่อยๆ </span>
			 				เพื่อให้เห็นการเคลื่อนไหวของฮีโร่ในเกม
			 			</li>
			 			<li>
			 				เกมนี้เล่นเป็นทีม ชนะเป็นทีม
			 				<span className={styles['text--red']}> อย่าฉายเดี่ยว </span>
			 				สื่อสารกับทีมให้รู้เรื่อง
			 			</li>
			 			<li>
			 				<span className={styles['text--red']}>สู้เมื่อพร้อม </span>
			 				(เลือด, มานาเต็ม, สกิลพร้อม)
			 			</li>
			 			<li>
			 				<span className={styles['text--red']}>อย่าตายบ่อย!</span>
			 			</li>
			 		</ul>
			 	</div>
			</div>
			<div className={styles['charbg_murad']}/>
		</div>
	);
}