import React from 'react';
import styles from './hero_detail.module.scss'
import {Imglist} from './../../constants/import_image';
export default props=>{
	return(
		<div className={styles['detail']}>
			<div className={styles['detail__container']}>
				 <div className={styles['detail__content']}>
				 	ฮีโร่ตัวละครหลักของเกม <span className={styles['text--red']}>ผู้เล่นแต่ละคนจะเล่นฮีโร่ 1 ตัวในแต่ละเกม</span>
				 	ในช่วงเริ่มเกม ฮีโร่แต่ละตัวจะเริ่มจากไม่มีค่าประสบการณ์ (EXP) นั่นคือ เลเวล 1 และ
				 	<span className={styles['text--red']}>เลเวลจะเพิ่มขึ้นจากการหาออบเจ็กต์ต่างๆ</span> เพื่อเก็บค่าประสบการณ์จนมีเลเวล
				 	สูงสุดที่เลเวล 15
				 </div>
				 <div className={styles['detail__imgwrap']}>
				 		<div className={styles['detail__imgwrap--each']}>
				 			<div className={styles['detail__imgwrap--text']}>ต้นเกม</div>
				 			<div className={styles['detail__imgwrap--imgbox']}>
				 			 	<img src={Imglist['h1']} alt=""/>
				 			 </div>
				 			<div className={styles['detail__imgwrap--text']}>
				 				<span className={styles['text--red']}>Lv.1 </span> = ฆ่าครีป เก็บเลเวล เก็บเงิน
				 			</div>
				 		</div>
				 		<div className={styles['arrow']}>
				 			<img src={Imglist['arrow']} alt=""/>
				 		</div>
				 		<div className={styles['detail__imgwrap--each']}>
				 			<div className={styles['detail__imgwrap--text']}>กลางเกม</div>
				 			<div className={styles['detail__imgwrap--imgbox']}>
				 			 	<img src={Imglist['h2']} alt=""/>
				 			 </div>
				 			<div className={styles['detail__imgwrap--text']}>
				 				<span className={styles['text--red']}>Lv.เพิ่มขึ้น </span> = ฆ่าครีป ฆ่ามอนสเตอร์ ซ่าฮีโร่ (เข้าร่วมต่อสู้)
				 			</div>
				 		</div>
				 		<div className={styles['arrow']}>
				 			<img src={Imglist['arrow']} alt=""/>
				 		</div>
				 		<div className={styles['detail__imgwrap--each']}>
				 			<div className={styles['detail__imgwrap--text']}>ท้ายเกม</div>
				 			<div className={styles['detail__imgwrap--imgbox']}>
				 			 	<img src={Imglist['h3']} alt=""/>
				 			 </div>
				 			<div className={styles['detail__imgwrap--text']}>
				 				<span className={styles['text--red']}>Lv.15 </span> = สร้างความเสียหายให้กับป้อม และฮีโร่เพื่อจบเกม
				 			</div>
				 		</div>
				 </div>
			 </div>
		</div>
	);
}