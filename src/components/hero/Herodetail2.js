import React from 'react';
import styles from './hero_detail.module.scss'
import {Imglist} from './../../constants/import_image';
export default props=>{
	return(
		<div className={styles['detail']}>
			<div className={styles['detail__container']}>
				 <div className={styles['detail2__content']}>
				 	ฮีโร่หนึ่งตัวจะมี
				 	<span className={styles['text--red']}>ค่าพลังชีวิต (HP) </span> 
				 	(ค่าพลังชีวิต แสดงถึงการมีชีวิตของฮีโร่) และ 
				 	<span className={styles['text--red']}>มานา (MP) </span>
				 	(ค่าที่ต้องใช้แลก เมื่อใช้สกิล หากเหลือมานาเยอะ ก็จะทําให้ใช้สกิลได้บ่อยขึ้น)
				 </div>
				 <div className={styles['detail2__contentwrap']}>
					 <div className={styles['detail2__content--imgbox']}>
					 	<img src={Imglist['h4']} alt=""/>
					 	{/*<div className={styles['detail2__arrow'] + ' ' + styles['detail2__arrow--top']}>
					 		<img src={Imglist['arrow_top']} alt=""/>
					 	</div>
					 	<div className={styles['detail2__arrow'] + ' ' + styles['detail2__arrow--bottom']}>
					 		<img src={Imglist['arrow_bottom']} alt=""/>
					 	</div>*/}
					 </div>
					 <div className={styles['detail2__textbox']+ ' ' + styles['detail2__textbox--top']}>
					 	 <span className={styles['text--blue']}>พลังชีวิต (HP)</span> หากโดนโจมตีจนเหลืือ 0 ฮีโร่จะตาย โดยฮีโร่จะอยู่ในสถานะ 
					 	 การตายในช่วงเวลาที่กําหนด และจะฟื้นคืนชีพมาเล่นต่อได้ตามปกติ ซึ่ง
					 	 <span className={styles['text--blue']}>ระยะเวลารอเกิดใหม่ </span>
					 	 ขึ้นอยู่กับเลเวล ยิ่งเลเวลเยอะยิ่งรอนาน<br/>
					 	 - แถบสีด้านบนของฮีโร่แต่ละตัว จะบ่งบอกฝ่ายของตัวละครได้ด้วย 
					 	 	คือ สีเขียว = ตัวเราเอง, สีน้ําเงิน = เพ่ือน, สีแดง = ศัตรู
					 </div>
					 <div className={styles['detail2__textbox'] + ' ' + styles['detail2__textbox--bottom']}>
					 		<span className={styles['text--blue']}>มานา (MP)</span> = ค่าที่ต้องใช้แลก เมื่อใช้สกิล <span className={styles['text--blue']}>ถ้ามานาหมด 
					 		จะไม่สามารถใช้สกิลได้</span> (การที่มีมานาเยอะ ไม่ใด้ทําให้สกิลแรงขึ้น แต่จะทําให้เราใช้สกิลได้บ่อยข้ึน)
					 </div>
				 </div>
				 <div className={styles['detail2__footer']}> 
					 เลือด และมานาจะได้รับการฟื้นฟู (มีค่าเพิ่มขึ้น)<span className={styles['text--red']}> เมื่อฮีโร่ไม่ได้รับการโจมตีหรือไม่ได้ใช้มานา</span>
				</div>
			 </div>
		</div>
	);
}