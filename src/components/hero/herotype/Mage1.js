import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ป้องกันป้่อม',
		'content':'ป้องกันศัตรูมาโจมตีป้อมเรา โดยการรีบฆ่าครีปฝั่งศัตรูให้เร็วที่สุดก่อนที่ครีปจะเข้าสู่ระยะป้อม'
	},
	{
		'title': 'แกงค์ ',
		'content':'ช่วยเพื่อนต่อสู้กับศัตรู'
	},
	{	
		'title': 'เติมดาเมจ',
		'content':'ร่วมสร้างความเสียหายให้กับศัตรู โดยการโจมตีหรือใช้สกิล'
	},
	{
		'title': 'ทำดาเมจ  ',
		'content':'การสร้างความเสียหายให้แก่ศัตรู',
	},	
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					เป็นตัวทำดาเมจเวทจากแนวหลัง
					<span className={styles['text--red']}> มีพลังโจมตีเวทรุนแรง </span>
					มีสกิลในการขจัดจังหวะศัตรู
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['mage_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['mage_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_mage_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['mage_icon']} alt=""/>
											ตำแหน่งการยืน เมจ (Mage)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['mage_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>“ยืนเลนกลาง โจมตีเวท เน้นสกิล”</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<span className={styles['text--title']}>รูปแบบการต่อสู้ร่วมกับทีม</span>
								<li>ยืนหลัง ๆ เป็นดาเมจหลักในการต่อสู้ (จากระยะไกล)</li>
								<li>พยายามใช้สกิลให้ได้เยอะที่สุด เน้นโจมตี Tank และ Fighter ฝั่งศัตรู</li>
								<li>จังหวะต่อสู้ให้ระวังแอสซาซินฝ่ายตรงข้าม</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['flicker']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['heal']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['purify']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_mage_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}