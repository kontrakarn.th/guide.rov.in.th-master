import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ไอเทม',
		'content':'ของที่จะทำให้ฮีโร่มีความสามารถเพิ่มขึ้น เช่น มีพลังโจมตีสูงขึ้น'
	},
	{
		'title': 'มอนสเตอร์ป่า',
		'content':'ฮีโร่สายแทงค์จะคอยเดินเช็คมอนสเตอร์ป่า เช่น Dark Slayer เพราะเมื่อเราฆ่ามอนสเตอร์เหล่านี้ได้ เราจะได้พลังพิเศษในช่วงขณะหนึ่ง ทำให้ทีมได้เปรียบในการต่อสู้'
	},
	{	
		'title': 'เลน Dark Slayer',
		'content':'เลนที่อยู่ใกล้ Dark Slayer และมังกรเล็ก'
	},
	{
		'title': 'เลนมังกรใหญ่',
		'content':'เลนที่อยู่ใกล้ Abyssal Dragon'
	},
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					เป็นตัว<span className={styles['text--red']}>รับดาเมจหลัก</span>
					จากการโจมตีของศัตรู เน้นปกป้องเพื่อน เพราะมี
					<span className={styles['text--red']}>พลังชีวิตสูง</span>
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['tank_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['tank_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_tank_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['tank_icon']} alt=""/>
											ตำแหน่งการยืน แทงค์ (Tank)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['tank_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>"เปิดไฟท์ ยืนหน้า เสียสละ"</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<span className={styles['text--title']}>รูปแบบการต่อสู้ร่วมกับทีม</span>
								<li>เป็นตัวเปิดการต่อสู้ (ควรรอให้ทีมมาพร้อมก่อน)</li>
								<li>พยายามป้องกัน และรับการโจมตีจากศัตรูให้ได้นานที่สุด</li>
								<li>แม้จะรับดาเมจได้เยอะ แต่ก็ไม่ควรทำให้ตัวเองเสี่ยงตายอย่างเปล่าประโยชน์</li>
								<li>พยายามสอดส่องศัตรู และบอกเพื่อนร่วมทีมว่าศัตรูกำลังทำอะไรอยู่</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['heal']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['flicker']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_tank_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}