import React from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'

export default props=>{
	return(
		<div className={styles['wrap--top']}>
			<div className={styles.container}>
				<div className={styles['role__gallery']}>
					<img src={Imglist['fighter']} alt=""/>
				</div>
			</div>
		</div>
	);
}