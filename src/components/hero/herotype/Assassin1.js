import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ไอเทม',
		'content':'ของที่จะทำให้ฮีโร่มีความสามารถเพิ่มขึ้น เช่น มีพลังโจมตีสูงขึ้น'
	},
	{
		'title': 'ดาเมจ ',
		'content':'การสร้างความเสียหายให้แก่ศัตรู'
	},
	{	
		'title': 'เดินแกงค ',
		'content':'ช่วยเพื่อนต่อสู้กับศัตรูในเลนต่าง ๆ'
	},
	{
		'title': 'ยืนหลบ ',
		'content':'ยืนให้ฝั่งศัตรูไม่เห็น หรือไม่สามารถโจมตีเราได้'
	},
	{
		'title': 'ลอบฆ่า  ',
		'content':'การฝ่าวงศัตรู เพื่อลอบฆ่า'
	},
	{
		'title': 'ล้วง   ',
		'content':'การอ้อมไปเล่นงานฮีโร่ที่อยู่ในแนวหลัง ของฝ่ายศัตรู'
	},
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					คอย<span className={styles['text--red']}>ลอบสังหารศัตรู </span>
					เน้นการเคลื่อนที่ว่องไวมี
					<span className={styles['text--underline']}>ดาเมจ</span>
					ระยะประชิดที่รุนแรง แต่
					<span className={styles['text--red']}>มีพลังชีวิตตํ่า</span>
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['assassin_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['assassin_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_assassin_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['assassin_icon']} alt=""/>
											ตำแหน่งการยืน แอสซาซิน (Assassin)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['assassin_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>“เดินในป่า ล้วงตัวบาง พรางในพุ่ม”</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']} style={{top:'-1rem'}}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<span className={styles['text--title']}>รูปแบบการต่อสู้ร่วมกับทีม</span>
								<li>ฆ่าตัวทำดาเมจฝั่งศัตรู <span className={styles['text--underline']}>(ล้วง)</span></li>
								<li>หาพื้นที่เพื่อล้วงศัตรู อยู่ในจุดที่ศัตรูป้องกันตัวได้ยาก</li>
								<li>พยายามเคลื่อนไหวตลอดเวลา เพื่อป้องกันการตกเป็นเป้าของศัตรู</li>
								<li>ดูจังหวะก่อนเข้าต่อสู้ให้ดี เพื่อทำดาเมจ และป้องกันตัวเองให้ไม่ตาย</li>
								<li>ป้องกันแครี่ ขัดจังหวะการโจมตีของแอสซาซินฝั่งศัตรู</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['punish']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['execute']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_assassin_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}