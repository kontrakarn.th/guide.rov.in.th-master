import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'สตั้น',
		'content':'สกิลที่จะทำให้ศัตรูไม่สามารถเคลื่อนที่ และโจมตีได้'
	},
	{
		'title': 'สโลว์ ',
		'content':'สกิลที่จะลดความเร็วในการเคลื่อนที่ศัตรูได้'
	},
	{
		'title': 'ใบ้',
		'content':'สกิลที่จะทำให้ศัตรูไม่สามารถใช้สกิลได้'
	},
	{
		'title': 'ฮีล',
		'content':'การฟื้นฟูพลังชีวิต'
	},
	{
		'title': 'ทำจังหวะ',
		'content':'สร้างโอกาสในการฆ่าศัตรู ทำให้ทีมได้เปรียบในการต่อสู้มากขึ้น ด้วยการใช้สกิลทำให้ศัตรูมึน หรือลดความเร็ว ฯลฯ'
	},
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					เป็น<span className={styles['text--red']}>ตัวที่ช่วยทำให้เพื่อนเล่นเกมง่ายขึ้น</span>
					เช่น คอยดูว่าศัตรูอยู่ไหน,<span className={styles['text--underline']}>ฮีล</span>
					เพื่อน ๆ ใช้สกิลในการปั่นป่วนศัตรู เช่น สกิล 
					<span className={styles['text--underline']}>สตั้น</span>,
					<span className={styles['text--underline']}>สโลว์</span>,
					<span className={styles['text--underline']}>ใบ้</span>
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['support_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['support_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_support_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['support_icon']} alt=""/>
											ตำแหน่งการยืน ซัพพอร์ท (Support)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['support_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>“คอยดูแล เปิดพื้นที่ ขัดศัตรู”</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<li>ช่วยสนับสนุนการต่อสู้ และสนับสนุนเพื่อนในทีมทุกคน</li>
								<li>ช่วยเพื่อนในทีมให้ไม่ตาย</li>
								<li>คอยสอดส่องแอสซาซินฝั่งศัตรู</li>
								<li>พยายามสอดส่องศัตรู และบอกเพื่อนร่วมทีมว่าศัตรูกำลังทำอะไรอยู่</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['heal']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['flicker']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_support_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}