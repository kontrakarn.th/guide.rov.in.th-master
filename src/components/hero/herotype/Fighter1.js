import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ทำดาเมจ',
		'content':'การสร้างความเสียหายให้แก่ศัตรูู'
	},
	{
		'title': 'เติมดาเมจ',
		'content':'ร่วมสร้างความเสียหายให้กับศัตรู โดยการโจมตี หรือใช้สกิล'
	},
	{	
		'title': 'ตัวทำดาเมจหลัก ',
		'content':'ฮีโร่ที่สามารถสร้างความเสียหายเป็นจำนวนมากให้กับฝ่ายศัตรู'
	},
	{
		'title': 'กดดันเลน',
		'content':'การโจมตีฮีโร่ฝ่ายศัตรู เพื่อให้พลังชีวิตของฮีโร่ฝ่ายศัตรูลดลง ในขณะที่ครีปของแต่ละฝั่งทำการต่อสู่้กัน และทำการโจมตีป้อมของฝ่ายศัตรู'
	},
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					เป็นตัวหลักในการ<span className={styles['text--underline']}>ทำดาเมจ</span>
					ระยะประชิด เพราะ
					<span className={styles['text--red']}>มีพลังชีวิตสูง และพลังการโจมตีระยะประชิดสูง </span>
					(มีความสามารถกึ่งกลางระหว่างแทงค์ และแอสซาซิน)
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['fighter_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['fighter_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_fighter_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['fighter_icon']} alt=""/>
											ตำแหน่งการยืน ไฟต์เตอร์ (Fighter)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['fighter_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>“เด่นรุกรับ เล่นยืดหยุ่น เน้นแยกดัน”</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<span className={styles['text--title']}>รูปแบบการต่อสู้ร่วมกับทีม</span>
								<li>พยายามหาจังหวะเข้าโจมตี<span className={styles['text--underline']}>ตัวทำดาเมจหลัก</span>ของศัตรู เช่น เมจ, แครี่ และ แอสซาซิน</li>
								<li>คอยสนับสนุนการโจมตี และป้องกันให้กับทีม</li>
								<li>มุ่งเน้นการคุมเลน สร้างพื้นที่ให้เพื่อนได้เดินเกม</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['execute']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['flicker']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_fighter_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}