import React,{Fragment} from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'
import Popupbtn from './../../../components/navigation/button/Popupbtn'

const message = 
[
	{
		'title': 'ทําดาเมจกายภาพ',
		'content':'การโจมตีธรรมดา'
	},
	{
		'title': 'ฟาร์ม',
		'content':'การฆ่าครีป และมอนสเตอร์ เพื่อเก็บเลเวล และเงิน'
	},
]

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>หน้าที่</span>:
					เป็นตัวหลักในการ<span className={styles['text--underline']+' '+ styles['text--red']}>ทำดาเมจกายภาพ</span>
					<span className={styles['text--red']}>ระยะไกล</span> ที่รุนแรง					
				</div>
				<div>
					<div className={styles['imgwrap__phase']}>
						<img src={Imglist['carry_phase']} alt=""/>
					</div>
					<div className={styles['map']}>
						<div className={styles['map__title']}>ตำแหน่งการยืน</div>
						<div className={styles['imgwrap__map']}>
							<img src={Imglist['carry_map']} alt=""/>
							<Popupbtn instyle={{'right':'-5%','bottom':'-5%','zIndex':'200'}} img={Imglist['zoom']} imgstyle={{'maxWidth':'60%'}} id={'hero_carry_map'}>
									<div className={styles['role__modal']}>
										<div className={styles['role__modal--title']}>
											<img src={Imglist['carry_icon']} alt=""/>
											ตำแหน่งการยืน แครี่ (Carry)
										</div>
										<div className={styles['role__modal--img']}>
											<img src={Imglist['carry_pos']} alt=""/>
										</div>
									</div>
							</Popupbtn>
						</div>
						<div className={styles['map__text']}>
							<span className={styles['text--red']}>“ยืนให้เป็น เล่นให้รวย ฆ่าให้เรียบ”</span>
						</div>
					</div>
				</div>
				<div className={styles['botcontent']}>
					<div className={styles['botcontain']}>
						<div className={styles['borderdot']}>
							<ul className={styles['botcontain__list']}>
								<li>ต้องทำดาเมจหลักให้กับทีม โดยเฉพาะช่วงท้ายของเกม</li>
								<li>ระวังการโจมตีระยะประชิดจาก Assassin ฝ่ายตรงข้าม</li>
								<li>พยายามเอาตัวรอดจนจบการต่อสู้</li>
								<li>ห้ามตายบ่อย เพราะทีมจะขาดตัวทำดาเมจ</li>
							</ul>
						</div>
					</div>
					<div className={styles['challenger']}>
						<div>Challenger Skill แนะนำ</div>
						<div className={styles['challenger__wrap']}>
							<div className={styles['challenger__img']}>
								<img src={Imglist['flicker']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['punish']} alt=""/>
							</div>
							<div className={styles['challenger__img']}>
								<img src={Imglist['roar']} alt=""/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'hero_carry_content'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</div>
	);
}