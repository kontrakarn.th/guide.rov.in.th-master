import React from 'react'
import styles from './herotype.module.scss'
import {Imglist} from './../../../constants/import_image'

export default props=>{
	return(
		<div className={styles['wrap']}>
			<div className={styles.container}>
				<div className={styles['role__gallery']}>
					<img src={Imglist['carry']} alt=""/>
				</div>
			</div>
		</div>
	);
}