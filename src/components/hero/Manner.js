import React from 'react'
import styles from './hero_detail.module.scss'

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['manner__container']}>
			 	<div className={styles['manner__contentbox']}>
			 		<ul>
			 			<li>
			 				<span className={styles['text--red']}>ไม่ AFK </span>
			 				- ควรเตรียมตัวให้เพร้อมก่อนเล่น
			 			</li>
			 			<li>
			 				<span className={styles['text--red']}>ไม่ออก</span>
			 				ระหว่างเกม
			 			</li>
			 			<li>
			 				<span className={styles['text--red']}>ไม่ด่า </span>
			 				ไม่หยาบคาย
			 				<span className={styles['text--red']}> เห็นใจเพื่อน</span>
			 				ร่วมทีม
			 			</li>
			 			<li>
			 				เลือกฮีโร่ให้
			 				<span className={styles['text--red']}>สมดุล</span>
			 				กับทีม
			 			</li>
			 		</ul>
			 		<div className={styles['manner__contentbox--box'] + ' ' + styles['borderdot']}>
			 			<div className={styles['text--title']}>บทลงโทษ</div>
			 			<div>
			 				ไม่ได้รับรางวัลประจำฤดูกาล และจะไม่ได้รับบันทึกคะแนนในระบบ
			 				<span className={styles['text--red']}> หากกระทำผิดซํ้าจะถูกรายงาน</span>
			 				ชื่อบนเว็บไซต์หลัก หรือแม้กระทั่ง
			 				<span className={styles['text--red']}>ถูกแบนไอดี</span>
			 			</div>
			 		</div>
			 		<div className={styles['manner__footer']}>
			 			<span className={styles['text--red']}>AFK (Away From Keyboard) </span>
			 			คือ การที่ผู้เล่นไม่เล่นเกม
			 		</div>
			 	</div>
			</div>
			<div className={styles['charbg']}/>
		</div>
	);
}