import React from 'react';
import styles from './overview.module.scss';
import {Imglist} from './../../../constants/import_image.js';

export default props=>{
	return(
		<div className={styles['overview']}>
			<div className={styles['overview__container']}>
				<div className={styles['overview__content']}>
					ฮีโร่แต่ละตัวจะมีลักษณะ และ<span className={styles['text--red']}>ความสามารถแตกต่างกัน</span>ไป <br/>
					คุณสมบัติของฮีโร่เบื้องต้น ถูกจัดตามคุณสมบัติของฮีโร่ส่วนใหญ่ในสายนั้นิ				
				</div>
				<div className={styles['overview__imgwrap']}>
					<img src={Imglist['hero_overview']} alt=""/>
				</div>
				<div className={styles['overview__footer']}>
					ดาเมจ (Damage) = <span className={styles['text-red']}>ค่าความเสียหาย</span>
				</div>
			</div>
		</div>
	);
}	