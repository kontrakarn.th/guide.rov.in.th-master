import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
// import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent']}>
						<ul>
							<li>
								<span className={styles['text--red']}>เมื่อเรามีเงินครบ</span>
								ตามราคาของไอเทมที่เราได้เซ็ตไว้ก่อนเริ่มเกม ไอเทมจะปรากฏขึ้น
							</li>
							<li>
								<span className={styles['text--red']}>สำหรับผู้เล่นใหม่ </span>
								สามารถออกของตาม Guide (เซ็ตไอเทมแนะนำ) ได้
							</li>
						</ul>
					</div>
					<div className={styles['purchaseitem__contentmid']}>
						<img src={Imglist['howto']} alt=""/>
					</div>
				</div>
			</div>
	)
}