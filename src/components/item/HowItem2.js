import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent']}>
						<ul>
						เราสามารถ
						<span className={styles['text--red']}>ซื้อไอเทมล่วงหน้า</span>ได้หากต้องการเรียงลำดับในการซื้อ
						ไอเทมเอง โดยคลิกไปที่รูปรถเข็นในเกม (ร้านค้า) และเมื่อมีเงินครบตามราคาของไอเทม ไอเทมจะปรากฏออกมาให้เรา
						ซื้อได้ทันที ตรงด้านซ้ายของหน้าจอ
						</ul>
					</div>
					<div className={styles['howto__contentmid']}>
						<img src={Imglist['howto2']} alt=""/>
					</div>
					<Popupbtn cusclass={'howtopop'} img={Imglist['icon']} id={'item_howto2_popup'}>
								<div className={styles['purchasepop']}>
									<div className={styles['purchasepop__imgwrap']}>
										<img src={Imglist['modal_howto']} alt=""/>
									</div>
									<div className={styles['purchasepop__content']}>
										เราสามารถขายไอเทมคืนได้ โดยจะ<br/>
										<span className={styles['text--dblue']}>ได้เงินคืนมา 60% ของราคาที่ซื้อไป </span> 
									</div>
								</div>
					</Popupbtn>
				</div>
			</div>
	)
}