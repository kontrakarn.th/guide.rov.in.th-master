import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
// import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
		<div className={styles['wrapper__tips']}>
			<div className={styles['container']}>
				<div className={styles['topcontent'] + ' ' + styles['topcontent__tips']}>
					<ul>
						<li>
							ไอเทมสกิลตัวที่มี<span className={styles['text--red']}>สกิลติดตัวชื่อเหมือนกัน จะแสดงผลแค่ชิ้นเดียว</span>
						</li>
						(การมีไอเทม 2 ชิ้นที่มีสกิลติดตัวเหมือนกัน <span className={styles['text--red']}> ไม่ได้เพิ่มความสามารถ x2</span>)
					</ul>
				</div>
				<div className={styles['tips__itembox']}>
						<img src={Imglist['tips1']} alt=""/>
				</div>
			</div>
			<div className={styles['container']}>
				<div className={styles['topcontent'] + ' ' + styles['topcontent__tips']}>
					<ul>
						<li>
							คนที่เล่นป่า เมื่อพกสกิล Punish มา หากอัพเกรด Hunter's Crossbow จะ<span className={styles['text--red']}>สามารถใช้ Frostbit (Punish หลังจากอัพเกรด Hunter's Crossbow) ใส่ฮีโร่ได้</span>
						</li>
						(การมีไอเทม 2 ชิ้นที่มีสกิลติดตัวเหมือนกัน <span className={styles['text--red']}> ไม่ได้เพิ่มความสามารถ x2</span>)
					</ul>
				</div>
				<div className={styles['tips__itembox']}>
						<img src={Imglist['tips2']} alt=""/>
				</div>
			</div>
		</div>
	)
}