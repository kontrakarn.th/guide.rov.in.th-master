import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent'] + ' ' + styles['topcontent__tips']}>
						<ul>
							<li>
								การป้องกันกายภาพ และเวท <span className={styles['text--red']}> มีผลแยกกัน </span>
								ดังนั้นควรออกเกราะให้ถูกประเภทกับคู่ต่อสู้ที่เจอด้วย
							</li>
							(ตัวอย่างเปรียบเทียบค่าคุณสมบัติของฮีโร่ เมื่อออกไอเทมป้องกันที่ต่างกัน)
						</ul>
					</div>
					<Popupbtn cusclass={'tipsmodal'} img={Imglist['tips_box']} imgstyle={{'maxWidth':'100%'}} id={'item_tips_popup'}>
								<div className={styles['tips__itembox']}>
										<img src={Imglist['tips_box']} alt=""/>
								</div>
					</Popupbtn>
					<div className={styles['item__footer']}>
						เราสามารถออกเกราะ
						<span className={styles['text--red']}>หลายอันผสมกัน</span>
						ได้
					</div>
				</div>
				<div className={styles['charbg'] + ' '+ styles['charbg__taara']}/>
			</div>
	)
}