import React,{Fragment} from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
import Popupbtn from './../../components/navigation/button/Popupbtn'

const itemlist = [Imglist['damage'],Imglist['magic_damage'],Imglist['defense'],Imglist['movement'],Imglist['jungle_item'],Imglist['support_item']]
const message = 
[
	{
		'title': 'ติดคริติคอล ',
		'content':'สร้างความเสียหายได้สองเท่าจากปกติ',
	},
	{
		'title': 'ดูดเลือด ',
		'content':'การโจมตี เลือดของศัตรูลดลง ในขณะที่เลือดของเราจะเพิ่มขึ้น',
	},
	{
		'title': 'มานา ',
		'content':'ค่าที่เราต้องใช้แลก เมื่อเราใช้สกิล ถ้ามานาหมดเราจะไม่สามารถใช้สกิลได้',
	},
	{
		'title': 'คูลดาวน์ ',
		'content':'ระยะเวลาที่ผู้เล่นจะต้องรอ ก่อนการใช้สกิลเดิมได้อีกครั้ง',
	},
	{
		'title': 'เกราะ ',
		'content':'ค่าป้องกันความเสียหายจากฝั่งตรงข้าม',
	},
]
export default props=>{
	return(
		<Fragment>
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent__itemtype']}>
						ประเภทของไอเทมมีทั้งหมด 6 ประเภท ซึ่งการใช้งานไอเทมมี 2 แบบ คือ
						<ul>
							<li>
								Passive คือ ไอเทมที่เมื่อซื้อมา จะ 
								<span className={styles['text--red']}>
									เพิ่มความสามารถให้ฮีโร่ได้อัตโนมัติ 
								</span>
								โดยไม่ต้องกดใช้งาน
							</li>
							<li>
								Active คือ ไอเทมที่เมื่อซื้อมา จะ 
								<span className={styles['text--red']}>
									ต้องกดใช้จึงจะเพิ่มความสามารถ
								</span>
								ให้กับฮีโร่
							</li>
						</ul>
					</div>
					<div className={styles['itemtype__itemcontainer']}>
						{
							itemlist.map((item,key) => {
								return(
									<Popupbtn instyle={{'display':'inline-block','position':'unset',width:'30%'}} img={item} imgstyle={{'maxWidth':'95%'}} key={key} id={'itemtype_'+(key+1)}>
										<div className={styles['itemtype__modal']}>
											<img src={item} alt=""/>
										</div>
									</Popupbtn>
								);
							})
						}
					</div>
				</div>
			</div>
			<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'item_typecontent'}>
				{
				 	message.map((item,key)=>{
						return(
							<Fragment key={key}>
								<span className={styles['text--blue']}>
									{item.title}
								</span>
								<span> = {item.content}</span><br/>
							</Fragment>
						)
					})
				}
			</Popupbtn>
		</Fragment>
	)
}