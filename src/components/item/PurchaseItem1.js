import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
// import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent']}>
						ในหนึ่งเกมเราจะสามารถมีไอเทมได้มากสุด 6 ชิ้น ซึ่งลำดับการซื้อไอเทมก่อน - หลังก็มีความสำคัญ
						เพราะไอเทมบางชิ้นเหมาะสำหรับต้นเกม
						<span className={styles['text--red']}> ถ้าเราซื้อไอเทมพลาด เราก็จะเสียเปรียบศัตรูตั้งแต่ต้นเกม</span>
					</div>
					<div className={styles['purchaseitem__contenttop']}>
						<img src={Imglist['itemset']} alt=""/>
					</div>
					<div className={styles['purchaseitem__contentmid']}>
						<img src={Imglist['purchase']} alt=""/>
					</div>
				</div>
			</div>
	)
}