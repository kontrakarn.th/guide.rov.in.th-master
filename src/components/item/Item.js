import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
// import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					<span className={styles['text--red']}>ไอเทม </span>
					คือ ของที่จะเพิ่มคุณสมบัติต่าง ๆ ให้เรา ช่วยให้เราได้เปรียบศัตรู เช่น ถ้าเรามีพลังโจมตีมากกว่าเราก็จะ
					 <span className={styles['text--red']}>มีโอกาสในการฆ่าศัตรูได้มากขึ้น</span>
					 (ตารางเปรียบเทียบ ฮีโร่ตัวเดียวกันตอนไม่มีไอเทม กับตอนที่มีไอเทมแล้ว)
				</div>
				<div className={styles['itembox']}>
					<img src={Imglist['item_box']} alt=""/>
				</div>
				<div className={styles['item__footer']}>
					การออกไอเทม, การออกของ = 
					<span className={styles['text--red']}> ซื้อไอเทม </span>
				</div>
			</div>
			<div className={styles['charbg']}/>
		</div>
	)
}