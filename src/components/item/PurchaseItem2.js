import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './item.module.scss'
import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent']}>
						เราสามารถเตรียมตัว
						<span className={styles['text--red']}>ซื้อไอเทมได้ตั้งแต่ก่อนเริ่มเกม </span>โดยกดปุ่ม
						<span className={styles['text--red']}> "ไอเทม" </span>
						และจัดชุดของไอเทมที่เราต้องการจะใช้ได้
						ซึ่งเราสามารถ
						<span className={styles['text--red']}>กำหนดเซ็ตของไอเทมได้ทั้งหมด 3 ชุด</span>
						ในฮีโร่แต่ละตัวของเรา และสามารถเซ็ตชุดของไอเทมเป็นค่าเริ้มต้นได้ด้วย
					</div>
					<div className={styles['purchaseitem__contentmid']}>
						<img src={Imglist['purchase2']} alt=""/>
					</div>
					<Popupbtn cusclass={'purchasepop'} img={Imglist['icon']} id={'item_purchase_popup'}>
								<div className={styles['purchasepop']}>
									<div className={styles['purchasepop__imgwrap']}>
										<img src={Imglist['modal_purchase']} alt=""/>
									</div>
									<div className={styles['purchasepop__content']}>
										เราสามารถดูคำแนะนำการออกไอเทมได้ โดยการกด 
										<span className={styles['text--dblue']}>“Suggested” (เซ็ตไอเทมแนะนำ) </span> 
										มีคำแนะนำจากโปร 
										พร้อมทั้งอัตราการชนะ ให้เราเลือกใช้ได้เลย
										(ทั้งนี้ทั้งนั้น การออกไอเทมที่ดีจะขึ้นอยู่กับรูปเกมด้วย)
									</div>
								</div>
					</Popupbtn>
				</div>
			</div>
	)
}