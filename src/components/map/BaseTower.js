import React from 'react';
import styles from './map.module.scss'
import {Imglist} from './../..//constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
					<div className={styles['topcontent']}>
						แต่ละฝั่งจะมีทั้งหมด 1 ฐาน 9 ป้อม แบ่งออกเป็น 3 เลน เลนละ 3ป้อม ได้แก่ 
						 <span className={styles['text--red']}>ป้อมช้ันนอก, ป้อมชั้นกลาง และป้อมช้ันใน ถ้าโจมตีป้อมฝ่ายศัตรูแตก ทั้งทีมจะได้ค่าประสบการณ์และเงิน</span>
					</div>
					<div className={styles['mapcontainer__new']}>
						<Popupbtn instyle={{'display':'block',margin:'0 auto','position':'unset'}} img={Imglist['base_new_content']} imgstyle={{'maxWidth':'100%'}} id={'map_basepopup'}>
								<div className={styles['mapcontainer__new--pop']}>
									<img src={Imglist['enlarge_content']} alt=""/>
								</div>
						</Popupbtn>
					</div>
			</div>
		</div>
	);
}	