import React from 'react';
import styles from './map.module.scss'
import {Imglist} from './../../constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'


export default props=>{

	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
					<div className={styles['topcontent']}>
						 ป่าจะถูกแบ่งเป็น 2 ส่วน คือ 
						<span className={styles['text--red']}>ป่าฝั่งเรา และป่าฝั่งศัตรู</span>
						ซึ่งถ้าหากเราอยู่ฝั่งตรงข้าม มุมมองก็จะยังคงเป็นเหมือนเดิมเว้นแต่ Dark Slayer, Abyssal Deagon 
						และ Spirit Sentinel จะ สลับฝั่งกัน
					</div>
					<div className={styles['mapcontainer']}>
						<img src={Imglist['bush_map']} alt=""/>
						<Popupbtn instyle={{'top':'46%','left':'46%','right':'unset','bottom':'unset','zIndex':'200'}} img={Imglist['exclamation_icon']} imgstyle={{'maxWidth':'40%'}} id={'map_bushpopup'}>
								<div className={styles['basepop']}>
									<div className={styles['basepop__imgwrap']}>
										<img src={Imglist['bush']} alt=""/>
									</div>
									<div className={styles['basepop__content']}>
									<span className={styles['text--blue']}>พุ่มไม้</span>
										= เมื่อเรายืนอยู่ในพุ่มไม้ ศัตรูที่อยู่ด้านนอกพุ่มไม้ จะไม่สามารถมองเห็นได้
										(จะมีสัญลักษณ์รูปตาขึ้นบนฮีโร่)
									</div>
								</div>
						</Popupbtn>
					</div>
					<span className={styles['text--red']}>คลิกที่สัญลักษณ์ <img className={styles['exclameimg']} src={Imglist['exclamation_icon']} alt=""/> เพื่อดูคำอธิบายเลนเพิ่มเติม </span>
			</div>
		</div>
	);
}	