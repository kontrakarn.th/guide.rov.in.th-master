import React from 'react';
import styles from './map.module.scss'
import {Imglist} from './../..//constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'


export default props=>{

	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
					<div className={styles['topcontent']}>
						 การยืนเลนมีความสำคัญมาก
						<span className={styles['text--red']}>หากเลือกยืนในตำแหน่งที่ไม่เหมาะสมกับประเภทฮีโร่ของเรา ก็อาจจะทำให้เสียเปรียบศัตรู และพ่ายแพ้ได้</span>
					</div>
					<div className={styles['mapcontainer']}>
						<img src={Imglist['map']} alt=""/>
						{/*<div className={styles['mapcontainer__box']}>*/}
							<Popupbtn instyle={{'top':'15%','left':'15%','right':'unset','bottom':'unset','zIndex':'200',width:'10%'}} img={Imglist['top_lane']} imgstyle={{'maxWidth':'100%',maxHeight:'100%'}} id={'map_lane_toppopup'}>
								<div className={styles['map_modal']}>
									<div className={styles['map_modal__title']}>
										<img src={Imglist['top_icon']} alt=""/>
										เลน Dark Slayer
									</div>
									<div className={styles['map_modal__content']}>
										<div className={styles['map_modal__content--left']}>
											<ul>
												<li>ช่วงแรกยังไม่มีการต่อสู้มากนัก เพราะ Dark slayer จะเกิดในนาทีที่ 6 
													ทำให้ไม่มีการปะทะกัน เพื่อแย่งมอนสเตอร์ในช่วงต้นเกม</li>
												<li>ฮีโร่ที่ยืนเลนนี้ควรเป็นฮีโร่ที่เคลียร์ครีปได้เร็ว เช่น แทงค์ ไฟต์เตอร์ 
													ที่มีสกิลเพิ่มเลือด มีพลังป้องกันสูง เพราะจะได้ไม่ต้องกลับบ้านบ่อย เช่น Skud</li>
												<li>ถ้าป้อมนอกสุดแตกแล้วจะเสียเปรียบ เพราะเราจะควบคุมพื้นที่ได้ยากขึ้น 
													(มีความเสี่ยงในการยืนเลนมากขึ้น เนื่องจากไม่มีป้อมช่วยคุ้มกัน)</li>
											</ul>
										</div>
										<div className={styles['map_modal__content--right']}>
												<img src={Imglist['top_map']} alt=""/>
										</div>
									</div>
								</div>
							</Popupbtn>
							<Popupbtn instyle={{'top':'45%','left':'40%','right':'unset','bottom':'unset','zIndex':'200',width:'10%'}} img={Imglist['mid_lane']} imgstyle={{'maxWidth':'100%',maxHeight:'100%'}} id={'map_lane_midpopup'}>
									<div className={styles['map_modal']}>
											<div className={styles['map_modal__title']}>
												<img src={Imglist['mid_icon']} alt=""/>
												เลนกลาง
											</div>
											<div className={styles['map_modal__content']}>
												<div className={styles['map_modal__content--left']}>
													<ul>
														<li>เป็นเลนที่เก็บเลเวลได้เร็ว เพราะครีปชนกันเร็วกว่าเลนอื่น
															(ระยะทางสั้นกว่าเลนอื่น) ทำให้มีครีปให้ฆ่าเร็วกว่าเลนอื่น</li>
														<li>ฮีโร่ที่ยืนเลนนี้ควรเป็นฮีโร่ที่สามารถสร้างดาเมจได้รุนแรงในระยะเวลาอันสั้น 
															เพื่อไปช่วยโจมตีเลนอื่นๆ เช่น เมจ หรือแครี่</li>
													</ul>
												</div>
												<div className={styles['map_modal__content--right']}>
														<img src={Imglist['mid_map']} alt=""/>
												</div>
											</div>
									</div>
							</Popupbtn>
							<Popupbtn instyle={{'right':'25%','bottom':'8%','zIndex':'200',width:'10%'}} img={Imglist['bot_lane']} imgstyle={{'maxWidth':'100%',maxHeight:'100%'}} id={'map_lane_botpopup'}>
									<div className={styles['map_modal']}>
											<div className={styles['map_modal__title']}>
												<img src={Imglist['bot_icon']} alt=""/>
												เลน Abyssal Dragon (เลนมังกรใหญ่)
											</div>
											<div className={styles['map_modal__content']}>
												<div className={styles['map_modal__content--left']}>
													<ul>
														<li>มีมังกรเกิดทุกๆ 3 นาที จึงเป็นเลนที่มีการต่อสู้บ่อยที่สุด เพื่อแย่งมังกร 
														เนื่องจากฆ่ามังกรแล้วจะได้เงิน และค่าประสบการณ์</li>	
														<li>ฮีโร่ต้องมีสกิลคุมพื้นที่ได้ เพื่อสร้างความได้เปรียบในการต่อสู้ เช่น Maloch </li>	
														<li>ถ้าป้อมเลนมังกรแตก จะทำให้เราควบคุมพื้นที่ฝั่งมังกรได้ยากขึ้น 
														และทำให้เสียมังกรให้ฝั่งตรงข้ามง่ายขึ้น</li>											
													</ul>
												</div>
												<div className={styles['map_modal__content--right']}>
														<img src={Imglist['bot_map']} alt=""/>
												</div>
											</div>
									</div>
							</Popupbtn>
						</div>
					{/*</div>*/}
					<span className={styles['text--red']}>คลิกที่สัญลักษณ์ <img className={styles['dotimg']} src={Imglist['dot']} alt=""/> เพื่อดูคำอธิบายเลนเพิ่มเติม </span>
					<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']} id={'map_lane_tips'}>
						<div className={styles['tips_modal']}>
							<div>
								<span className={styles['text--blue']}>
									คุมพื้นที่
								</span>
								<span> = สกิลที่สามารถใช้เป็นวงกว้างหรือสามารถใช้เพื่อป้องกันศัตรู เข้ามาในพื้นที่นั้น ๆ ได้</span><br/>
							</div>
							<div>
								<span className={styles['text--blue']}>
									เคลียร์ครีป, เคลียร์เวฟ
								</span>
								<span> = การฆ่าครีปทุกตัวที่กำลังเดินเข้าป้อมหรือปะทะกับครีปฝั่งเราอยู่</span><br/>
							</div>
						</div>
					</Popupbtn>
			</div>
		</div>
	);
}	