import React  from 'react';
import styles from './map.module.scss'
import {Imglist} from './../..//constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'

const monsterlist = 
	[	
		{
			title:'Sentry Hawk',
			icon: Imglist['sentry_hawk_icon'],
			imgModal: Imglist['sentry_hawk'],
			inStyle: {'top':'13%','left':'22%','right':'unset','bottom':'unset','zIndex':'200'},
			content1:'เกิดหลังจากเร่ิมเกม 30 วินาที และจะเกิดใหม่ทุกๆ 60 วินาที Sentry Hawk จะเคลื่อนท่ีอยู่ตลอดเวลา และจะไม่ตอบโต้กลับแม้ถูกโจมตี',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'จะทําให้ฮีโร่พันธมิตรที่อยู๋ในระยะที่กําหนดได้รับโกลด์ และ XP',
					'นอกจากนี้ดวงวิญญาณของ Sentry Hawk จะลอยเข้าสอดส่อง และเปิดพื้นที่ของป่าฝ่ายตรงข้าม',
				]
		},
		{
			title:'Dark Slayer',
			icon: Imglist['dark_slayer_icon'],
			imgModal: Imglist['dark_slayer'],
			inStyle: {'top':'29%','left':'26%','right':'unset','bottom':'unset','zIndex':'200'},
			content1:'เป็นมอนสเตอร์ที่จะเกิดในนาทีท่ี 8 หลังจากเร่ิมเกม และจะเกิดใหม่ทุกๆ 5 นาที ซ่ึงทั้ง 2 ทีมจะแย่งกันฆ่าในช่วงท้ายเกม เพราะเมื่อฆ่า Dark Slayer เราจะได้บัฟ ที่ทําให้แข็งแกร่งขึ้น เพื่อท่ีจะได้เปรียบศัตรู ช่วยให้เราดัน และกันป้อมได้ง่ายขึ้น',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'พลังการฟื้นฟู 1% ของพลังชีวิต และมานาทุกๆ 1 วินาที และจะทําให้ผลการฟื้นฟูจากฐาน ไม่โดนลดประสิทธิภาพหลังจากเพิ่งต่อสู้มา',
					'จะทําให้เราวิ่งเร็วจากฐาน มีประสิทธิภาพมากข้ึนเป็นเวลา 90 วินาท',
					'จะได้รับ Drake Mondester โดยต้องกดใช้เพื่อเรียกออกมา หน้าที่ของ Drake Mondester คือการวิ่งไปตามเลนท่ีใกล้ที่สุด แล้วจู่โจมครีป และป้อมปราการของศัตรู โดยเมื่อจู่โจมป้อมปราการ Drake Mondester ก็จะโดนความเสียหายด้วยเช่นกัน (ผู้ที่สังหาร Drake Mondester จะได้รับ 25 โกลด์)',
					'Dragon Howler : ขณะท่ี Drake Mondester ถูกเรียกออกมา ครีปจะได้รับพลังชีวิตเพิ่มขึ้น 25% เพ่ิมพลังโจมตี และความเร็วเคลื่อนที่ขึ้น 10% รวมถึงร่างกายของครีปจะใหญ่ข้ึน 10%',
				]
		},
		{
			title:'Spirit Sentinel (มังกรเล็ก)',
			icon: Imglist['spirit_sentinel_icon'],
			imgModal: Imglist['spirit_sentinel'],
			inStyle: {'top':'19%','left':'35%','right':'unset','bottom':'unset','zIndex':'200'},
			content1:'จะเกิดหลังจากผ่านไป 110 วินาที และจะเกิดใหม่ทุกๆ 2 นาที หลังโดนสังหาร',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'ได้รับการฟื้นฟูพลังชีวิต 50 หน่วย และเพิ่มความเร็วเคลื่อนที่ 20 หน่วย เมื่อไม่ได้ต่อสู้ระยะหนึ่งจะเพิ่มความเร็วอีก 40 หน่วยเป็นเวลา 60 วินาที',
				]
		},
		{
			title:'มอนสเตอร์ป่า',
			icon: Imglist['jungle_icon'],
			imgModal: Imglist['jungle'],
			inStyle: {'top':'26%','left':'47%','right':'unset','bottom':'unset','zIndex':'200'},
			content1:'เกิดหลังจากเริ่มเกม 28 วินาที และจะเกิดใหม่ทุกๆ 70 วินาที มอนสเตอร์ป่าจะอยู่กระจายกันท่ัวป่าท้ัง 2 ฝ่าย และหากถูกลากออกมาจากจุดเกิดมากเกินไป จะทําให้มันว่ิงกลับไปที่เดิมหลังช่วงเวลาหนึ่ง รวมถึงจะฟื้นฟูพลังชีวิตอย่างรวดเร็ว',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'จะทําให้ได้รับพลังชีวิตฟื้นคืนมาจํานวนหนึ่ง รวมถึงได้รับโกลด์ และ XP มากกว่าครีปทั่วไป',
				]
		},
		{
			title:'Sage Golem (บัฟฟ้า)',
			icon: Imglist['blue_icon'],
			imgModal: Imglist['blue'],
			inStyle: {'top':'37%','left':'unset','right':'23%','bottom':'unset','zIndex':'200'},
			content1:'เกิดหลังจากเริ่มเกม 30 วินาที และจะเกิดใหม่ทุกๆ 90 วินาที',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'ฮีโร่ท่ีสังหาร Sage Golem จะได้รับบัฟ ซ่ึงจะช่วยลดคูลดาวน์ให้กับสกิลและฟื้นฟูมานาอย่างต่อเนื่อง',
					'หากผู้ที่ครอบครองบัฟ ถูกสังหารโดยฮีโร่ฝ่ายตรงข้าม จะทําให้บัฟดังกล่าวถูกย้ายไปยังผู้ที่ทําการสังหารแทน และรีเซ็ตคูลดาวน์ใหม่'
				]
		},
		{
			title:'Abyssal Dragon (มังกรใหญ่)',
			icon: Imglist['abyssal_dragon_icon'],
			imgModal: Imglist['abyssal_dragon'],
			inStyle: {'top':'unset','left':'unset','right':'25%','bottom':'28%','zIndex':'200'},
			content1:'เม่ือเราฆ่ามังกรใหญ่ ใน RoV 2.0 นี้ ฮีโร่แต่ละสายจะได้รับผลบัฟแตกต่างกันออกไป ซึ่งจะทําให้ทีมได้เปรียบศัตรู ช่ัวขณะ',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'แทงค์ Tank : ฟื้นฟูพลังชีวิต 1% จากพลังชีวิตสูงสุด ต่อวินาที',
					'ไฟต์เตอร์ Fighter : พลังโจมตีเพิ่มขึ้น',
					'แอสซาซิน Assassin : ได้รับเจาะเกราะ และเจาะเกราะเวทเพ่ิมข้ึน นอกจากนี้จะเพิ่มความเร็วเคล่ือนท่ี 20 หน่วย',
					'เมจ Mage : เพิ่ม 75 + 15% พลังเวท',
					'แครี่ Carry : เพิ่ม 10% อัตราคริติคอล และ 15% เจาะเกราะ',
					'ซัพพอร์ท Support : ฟื้นฟู 100 พลังชีวิต และ 50 มานาแก่พันธมิตรในบริเวณใกล้เคียงทุกๆ 5 วินาที'
				]
		},
		{
			title:'Might Golem (บัฟแดง)',
			icon: Imglist['red_icon'],
			imgModal: Imglist['red'],
			inStyle: {'top':'unset','left':'unset','right':'42%','bottom':'22%','zIndex':'200'},
			content1:'เกิดหลังจากเร่ิมเกม 30 วินาที และจะเกิดใหม่ทุกๆ 90 วินาที',
			content2:'บัฟจากการสังหาร',
			subContent:
				[
					'ฮีโร่ที่สังหาร Might Golem จะได้รับบัฟโจมตี ที่จะช่วยเพ่ิมอัตราการสร้างความเสียหาย และลดความเร็วเคล่ือนท่ีของเป้าหมายลง',
					'เม่ือฮีโร่ท่ีโจมตีระยะไกลได้บัฟน้ี จะให้เอฟเฟคลดความเร็วเคล่ือนท่ีของคู่ต่อสู้เพ่ิมขึ้นตาม LV ตั้งแต่ 16% - 30%',
					'หากผู้ที่ครอบครองบัฟ ถูกสังหารโดยฮีโร่ฝ่ายตรงข้าม จะทําให้บัฟดังกล่าวถูกย้ายไปยังผู้ที่ทําการสังหารแทนและรีเซ็ตคูลดาวน์ใหม่'
				]
		},
	]
export default props=>{

	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
					<div className={styles['topcontent']}>
						 มอนสเตอร์ คือสิ่งมีชีวิตต่างๆ หลากหลายประเภทที่มีอยู่ในป่า
						<span className={styles['text--red']}>มอนสเตอร์แต่ละตัวจะให้เงิน และบัฟที่แตกต่างกัน</span>
						ออกไปซึ่งจะทำให้ฮีโร่ของเราได้รับความสามารถพิเศษ ในช่วงระยะเวลาหนึ่ง
					</div>
					<div className={styles['mapcontainer']}>
						<img src={Imglist['mon_map']} alt=""/>
						{
							monsterlist.map((item,key) => {
								return(
									<Popupbtn instyle={item.inStyle} img={item.icon} imgstyle={{'maxWidth':'60%'}} key={'monster'+key}>
										<div className={styles['map_modal']}>
											<div className={styles['map_modal__title']}>
												<img src={item.imgModal} alt=""/>
												{item.title}
											</div>
											<div className={styles['monster_modal']}>
													<ul className={styles['monster_modal__list']}>
														<li>{item.content1}</li>
														<li>
															{item.content2}
															<ul className={styles['monster_modal__sublist']}>
																{
																	item.subContent.map((text,index) => {
																		return(
																			<li key={'text'+ index}>{text}</li>
																		)
																	})
																}
															</ul>
														</li>
													</ul>
											</div>
										</div>
									</Popupbtn>
								)
							})
						}
					</div>
					{/*<div className={styles['monster__footer']}>
						<span className={styles['text--red']}>คลิกที่สัญลักษณ์ <img className={styles['dotimg']} src={Imglist['dot']} alt=""/> เพื่อดูคำอธิบายเลนเพิ่มเติม </span>
						<span><span className={styles['text--red']}>บัฟ =</span> พลังเพิ่มความสามารถในการต่อสู้ ในช่วงระยะเวลาหนึ่ง</span>
					</div>*/}
					<span className={styles['text--red']}>คลิกที่สัญลักษณ์ <img className={styles['dotimg']} src={Imglist['dot']} alt=""/> เพื่อดูคำอธิบายเลนเพิ่มเติม </span>
					<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']}>
						<div className={styles['monstertips_modal']}>
							<div className={styles['monstertips_modal__content']}>
								<p>
									<span className={styles['text--lightblue']}>บัฟ </span>
									= พลังเพ่ิมความสามารถในการต่อสู้ในช่วงระยะเวลาหนึ่ง
								</p>
								<p>
									• การฟาร์มมอนสเตอร์ป่า ควรพก 
									<span className={styles['text--lightblue']}> Spell Punish </span>
									มาใช้
									<span className={styles['text--lightblue']}> เพื่อการฟาร์มที่เร็วขึ้น</span>
								</p>
							</div>
							<div className={styles['monstertips_modal__imgwrap']}>
									<img src={Imglist['monster_punish']} alt=""/>
							</div>
						</div>
					</Popupbtn>
			</div>
		</div>
	);
}	