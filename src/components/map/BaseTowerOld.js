import React from 'react';
import styles from './map.module.scss'
import {Imglist} from './../..//constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'

baselist = 
	[	
		{
			title:'ป้อมนอก',
			icon: Imglist['tower_icon'],
			imgModal: Imglist['outer_tower'],
			inStyle: {'top':'43%','left':'38%','right':'unset','bottom':'unset','zIndex':'200'},
			content:'เป็นป้อมที่ช่วยให้เราเก็บเลเวล และเก็บเงินในช่วงต้นเกม โดยป้อมนอกของทุกเลน จะมีพยาบาล (เพ่ิม HP และ MP) อยู่หลังป้อม'
		},
		{
			title:'ป้อมกลาง',
			icon: Imglist['tower_icon'],
			imgModal: Imglist['mid_tower'],
			inStyle: {'top':'47%','left':'18%','right':'unset','bottom':'unset','zIndex':'200'},
			content:'ป้อมที่ช่วยควบคุมพื้นที่ป่า ทําให้เราฟาร์มป่าได้สะดวกขึ้น(ถ้าป้อมกลางแตก จะฟาร์มป่ายากขึ้น)'
		},
		{
			title:'ป้อมใน',
			icon: Imglist['tower_icon'],
			imgModal: Imglist['inner_tower'],
			inStyle: {'top':'unset','left':'34%','right':'unset','bottom':'17%','zIndex':'200'},
			content:'ป้อมสุดท้ายก่อนที่ศัตรูจะเข้าโจมตีฐาน ถ้าป้อมในแตก ครีปของฝ่ายศัตรู จะกลายเป็นซูปเปอร์ครีป มีเลือด และพลังโจมตีมากข้ึนและถ้าป้อมนี้แตกครบ 3 เลน ครีปจะยิ่งแข็งแกร่งขึ้น'
		},
		{
			title:'ฐาน',
			icon: Imglist['base_icon'],
			imgModal: Imglist['base'],
			inStyle: {'top':'unset','left':'18%','right':'unset','bottom':'21%','zIndex':'200'},
			content:'สามารถฟ้ืนฟูตัวเองได้ โดยจะฟ้ืนฟูในขณะที่ไม่ถูกโจมตีจากศัตรู ถ้าปกป้องฐานให้ดีๆ มันก็จะฟื้นฟูตัวเอง จนพลังเต็มหลอดได้เหมือนเดิม(ถ้าฐานของเราแตกคือแพ้)'
		},
		{
			title:'ป้อมนอกของฝ่ายศัตรู',
			icon: Imglist['e_tower_icon'],
			imgModal: Imglist['e_outer_tower'],
			inStyle: {'top':'35%','left':'unset','right':'35%','bottom':'unset','zIndex':'200'},
			content:'เป็นป้อมที่ช่วยให้ศัตรูเก็บเลเวล และเก็บเงินในช่วงต้นเกม หากเราทําลายป้อมนอกได้ก่อน ก็จะทําให้เราได้เปรียบศัตรู เพราะจะทําให้ศัตรู เก็บเลเวลได้ยากขึ้น'
		},
		{
			title:'ป้อมกลางของฝ่ายศัตรู',
			icon: Imglist['e_tower_icon'],
			imgModal: Imglist['e_mid_tower'],
			inStyle: {'top':'4%','left':'unset','right':'38%','bottom':'unset','zIndex':'200'},
			content:'ป้อมท่ีช่วยควบคุมพ้ืนที่ป่า ทําให้ศัตรูฟาร์มป่าได้สะดวกถ้าป้อมกลางแตก ศัตรูจะฟาร์มป่ายากขึ้น ทําให้เรามีโอกาสในการสังหารมากขึ้น'
		},
		{
			title:'ป้อมในของฝ่ายศัตรู',
			icon: Imglist['e_tower_icon'],
			imgModal: Imglist['e_inner_tower'],
			inStyle: {'top':'27%','left':'unset','right':'12%','bottom':'unset','zIndex':'200'},
			content:'ป้อมสุดท้ายก่อนท่ีเราจะเข้าโจมตีฐานของศัตรู ถ้าป้อมในแตก ครีปของเราจะกลายเป็นซูปเปอร์ครีป มีเลือด และพลังโจมตีมากข้ึน และถ้าป้อมนี้แตกครบ 3 เลน ครีปจะย่ิงแข็งแกร่งขึ้น'
		},
		{
			title:'ฐาน',
			icon: Imglist['e_base_icon'],
			imgModal: Imglist['e_base'],
			inStyle: {'top':'7%','left':'unset','right':'11%','bottom':'unset','zIndex':'200'},
			content:'คือเป้าหมายของเกม ถ้าโจมตีฐานศัตรูได้สำเร็จก็จะชนะ'
		},
		
	]
export default props=>{

	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
					<div className={styles['topcontent']}>
						 ทําหน้าท่ีโจมตีศัตรู ช่วยควบคุมพื้นท่ี ช่วยให้เราเก็บเลเวลและเก็บเงินได้ง่ายขึ้น 
						 <span className={styles['text--red']}>ถ้าโจมตีป้อมฝ่ายศัตรูแตก ท้ังทีมจะได้ค่า XP และเงิน </span>
						 โดยแต่ละฝั่ง จะมีทั้งหมด 1 ฐาน 9 ป้อม แบ่งออกเป็น 3 เลน เลนละ 3 ป้อม ได้แก่ 
						 <span className={styles['text--red']}>ป้อมช้ันนอก, ป้อมชั้นกลาง และป้อมช้ันใน</span>
					</div>
					<div className={styles['mapcontainer']}>
						<img src={Imglist['mon_map']} alt=""/>
						{
							baselist.map((item,key) => {
								return(
									<Popupbtn instyle={item.inStyle} img={item.icon} imgstyle={{'maxWidth':'100%'}} key={'tower'+key}>
										<div className={styles['basepop']}>
											<div className={styles['basepop__imgwrap']}>
												<img src={item.imgModal} alt=""/>
											</div>
											<div className={styles['basepop__content']}>
											<span className={styles['text--blue']}>{item.title}</span>
												{item.content}
											</div>
										</div>
									</Popupbtn>
								)
							})
						}
					</div>
					<span className={styles['text--red']}>คลิกที่สัญลักษณ์ <img className={styles['dotimg']} src={Imglist['dot']} alt=""/> เพื่อดูคำอธิบายเลนเพิ่มเติม </span>
					<Popupbtn instyle={{'right':'15%'}} img={Imglist['icon']}>
						<div className={styles['base_tips_modal']}>
							<ul>
								คำแนะนำ
								<li>ป้อมแต่ละป้อมมีพลังชีวิตต่างกัน ป้อมในสุด พลังชีวิตสูงสุด</li>
								<li>การโจมตีป้อมจะเรียงลําดับจาก นอก > ใน > ฐาน</li>
								<li>ป้อมจะเลือกโจมตีครีปก่อน</li>
								<li>ป้อมจะยิงแรงขึ้นเรื่อยๆ พยายามอย่าเข้าใกล้ป้อมศัตรูถ้าไม่มีครีป</li>
								<li>ถ้าโจมตีฮีโร่ศัตรูในระยะป้อม ป้อมจะทําการโจมตีเราทันที</li>
							</ul>
						</div>
					</Popupbtn>
			</div>
		</div>
	);
}	