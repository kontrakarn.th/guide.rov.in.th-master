import React from 'react'
import {Imglist} from './../../constants/import_image'
import styles from './summary.module.scss'

export default props=>{
	return(
			<div className={styles['wrapper']}>
				<div className={styles['container']}>
					<div className={styles['topcontent']}>
						<ul>
							<li>
								<span className={styles['text--red']}>การโจมตี </span>
								-
								<span className={styles['text--red']}>ตั้งค่าการควบคุมการโจมตีได้ตามความถนัด ในเมนู Settings (ตั้งค่า) - ควบคุม </span>
								ซึ่งสามารถตั้งค่าได้ทั้งจากในเกม และนอกเกม โดยมีให้เลือก 3 แบบ ได้แก่ โหมดออโต้ล็อค - โจมตีอัตโนมัติ - เลือกโจมตีครีปและป้อม และแบบเลือกเป้าหมาย
							</li>
						</ul>
					</div>
					<div className={styles['maincontent__setting']}>
							<img src={Imglist['setting_1']} alt=""/>
							<div className={styles['maincontent__setting--2']}>
									<ul>
										<li>
											<span className={styles['text--red']}>การปิดแชท </span>
											-
											การเถียงกันในเกมเป็นเรื่องที่ควบคุมไม่ได้ แต่เราป้องกัน ตัวเองได้ ด้วย
											<span className={styles['text--red']}>ปุ่มปิดห้องแชท ในเมนู Settings(ตั้งค่า) - ตั้งค่า UI</span>
										</li>
									</ul>
									<img src={Imglist['setting_2']} alt=""/>
							</div>
					</div>
				</div>
			</div>
	)
}