import React from 'react';
import styles from './summary.module.scss'
import {Imglist} from './../../constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'

const btnlist = 
	[
		{
			content:'รูปภาพของผู้เล่น',
			style:{'top':'2%','left':'0.5%','right':'unset','bottom':'unset','zIndex':'200'},
			id:'player_profile',
		},
		{
			content:'เช็คอินเพื่อรับของต่างๆ',
			style:{'top':'12%','left':'9%','right':'unset','bottom':'unset','zIndex':'200'},
			id:'checkin',
		},
		{
			content:'พบกับฮีโร่ สกิน และกิจกรรมใหม่ ๆ สุดพิเศษ',
			style:{'top':'15%','left':'25%','right':'unset','bottom':'unset','zIndex':'200'},
			id:'buy_btn',
		},
		{
			content:'เงิน เพชร และ คูปอง',
			style:{'top':'2%','left':'unset','right':'35%','bottom':'unset','zIndex':'200'},
			id:'user_money',
		},
		{
			content:'รายชื่อเพื่อน',
			style:{'top':'2%','left':'unset','right':'19%','bottom':'unset','zIndex':'200'},
			id:'friend_list',
		},
		{
			content:'กล่องจดหมาย',
			style:{'top':'2%','left':'unset','right':'13%','bottom':'unset','zIndex':'200'},
			id:'mailbox',
		},
		{
			content:'การตั้งค่าต่าง ๆ ภายในเกม',
			style:{'top':'2%','left':'unset','right':'7%','bottom':'unset','zIndex':'200'},
			id:'setting',
		},
		{
			content:'ความไวอินเตอร์เน็ต และ แบตเตอรี่ของเรา',
			style:{'top':'2%','left':'unset','right':'1%','bottom':'unset','zIndex':'200'},
			id:'network',
		},
		{
			content:'แถลงการพิเศษต่าง ๆ',
			style:{'top':'46%','left':'46%','right':'unset','bottom':'unset','zIndex':'200'},
			id:'event',
		},
		{
			content:'ทำภารกิจเพื่อรับของต่าง ๆ',
			style:{'top':'15%','left':'unset','right':'12%','bottom':'unset','zIndex':'200'},
			id:'mission',
		},
		{
			content:'แถลงการณ์พิเศษต่าง ๆ',
			style:{'top':'15%','left':'unset','right':'20%','bottom':'unset','zIndex':'200'},
			id:'event',
		},
		{
			content:'แถบเมนูร้านค้า การตั้งค่าฮีโร่ และกิลด์',
			style:{'top':'50%','left':'2%','right':'unset','bottom':'unset','zIndex':'200'},
			id:'sidemenu',
		},
		{
			content:'ลำดับแรงค์ของเพื่อน ๆ',
			style:{'top':'50%','left':'unset','right':'7%','bottom':'unset','zIndex':'200'},
			id:'friend_rank',
		},
		{
			content:'ของรางวัลสุดพิเศษ ในราคาคุ้มค่า',
			style:{'top':'unset','left':'13%','right':'unset','bottom':'12%','zIndex':'200'},
			id:'valorpass',
		},
		{
			content:'โหมดการเล่นต่าง ๆ',
			style:{'top':'unset%','left':'30%','right':'unset','bottom':'12%','zIndex':'200'},
			id:'play_normal',
		},
		{
			content:'โหมด 5V5',
			style:{'top':'unset','left':'unset','right':'43%','bottom':'12%','zIndex':'200'},
			id:'play5v5',
		},
		{
			content:'โหมดการแข่งขันเพื่อเก็บแรงค์',
			style:{'top':'unset','left':'unset','right':'20%','bottom':'12%','zIndex':'200'},
			id:'rankmode',
		},
		{
			content:'ช่องแชท',
			style:{'top':'unset','left':'20%','right':'unset','bottom':'1%','zIndex':'200'},
			id:'chat',
		},
		{
			content:'กิจกรรมต่าง ๆ ของเกม',
			style:{'top':'unset','left':'32%','right':'unset','bottom':'1%','zIndex':'200'},
			id:'eventbox',
		},
		{
			content:'ชมวิดีโอต่าง ๆ ของเกม',
			style:{'top':'unset','left':'unset','right':'17%','bottom':'1%','zIndex':'200'},
			id:'vdo',
		},

	]

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['topcontent']}>
				 คำอธิบายสัญลักษณ์ต่าง ๆ ในหน้าโฮม ว่าแต่ละอันใช้งานอย่างไร
				<span className={styles['text--red']}> เพื่อทำให้การเล่นเกิดประสิทธิภาพ</span>
				มากขึ้น
			</div>
			<div className={styles['container']}>
					<div className={styles['imgbgcontainer']}>
						<img src={Imglist['homeui_img']} alt=""/>
						<div className={styles['imgbgcontainer__cover']}>
							{
								btnlist.map((btn,key) => {
									return(
											<Popupbtn cusclass={'whitebtn'} instyle={btn.style} img={Imglist['icon']} imgstyle={{'maxWidth':'90%'}} id={btn.id} key={'homeuibtn' + key}>
												<div>
														{btn.content}
												</div>
											</Popupbtn>
									)
								})
							}
						</div>
					</div>
					<div className={[styles['text--red'],styles['botsection__text']].join(' ')}>คลิกที่สัญลักษณ์ <img className={styles['exclameimg']} src={Imglist['exclamation_icon']} alt=""/> เพื่อดูคำอธิบายเพิ่มเติม </div>
			</div>
		</div>
	);
}	