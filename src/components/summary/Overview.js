import React from 'react';
import styles from './summary.module.scss'
import {Imglist} from './../../constants/import_image'

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={styles['container']}>
				<div className={styles['topcontent']}>
					ผู้เล่นแต่ละคน จะบังคับฮีโร่คนละ 1 ตัว 
					<span className={styles['text--red']}>
						ฆ่าครีป มอนสเตอร์ ฮีโร่ หรือตีป้อม
					</span>
					เพื่อเก็บค่าประสบการณ์ และเงิน โดยต้องนำเงินที่ได้มาซื้อไอเทม ซึ่ง
					<span className={styles['text--red']}>
						การมีไอเทมมากกว่าศัตรูจะทำให้เราได้เปรียบ
					</span>
					เมื่อเราสามารถฆ่าศัตรูได้ ก็จะสามารถเข้าตีป้อมฝั่งตรงข้ามได้ ซึ่งถ้าเรา
					<span className={styles['text--red']}>
						ตีฐานแตกได้ ก็จะชนะ
					</span>
				</div>
				<div className={styles['maincontent']}>
					<img src={Imglist['overview_img']} alt=""/>
				</div>
				<div className={styles['botsection']}>
					<div className={styles['botsection__content']}>
						<img className={styles['botsection__iconcontent']} src={Imglist['overview_boticon']} alt=""/>
						<div>
							มีเงินเยอะ > มีไอเทมเยอะ > มีโอกาสชนะเยอะ
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

