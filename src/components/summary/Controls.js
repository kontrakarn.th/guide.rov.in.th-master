import React from 'react';
import styles from './summary.module.scss'
import {Imglist} from './../../constants/import_image.js'
import Popupbtn from './../../components/navigation/button/Popupbtn'

const btnlist = 
	[
		{
			content:`<span class='text--blue'>กดเพื่อขยายมินิแมพ</span>ให้มีขนาดใหญ่ขึ้น เพื่อทำสัญลักษณ์ต่างๆ<br/>
						ซึ่งสามารถส่งสัญญาณ บอกเพื่อนร่วมทีมได้ โดยกดค้างไปยังตำแหน่ง
						ที่เราต้องการจะส่งสัญญาณ โดยสัญญาณจะมี 3 แบบ ได้แก่<br/>
						- <span class='text--blue'>กำลังไป</span> ใช้เพื่อบอกเพื่อนร่วมทีมว่า เรากำลังจะเดินไปยังตำแหน่งไหน<br/>
						- <span class='text--blue'>ศัตรูหายไป</span>ใช้ตอนที่ศัตรูหายไปจากแมพ เพื่อเตือนให้เพื่อนร่วมทีม
						เพิ่มความระมัดระวังให้มากขึ้น<br/>
						- <span class='text--blue'>ถอย</span> ใช้ส่งสัญญาณให้เพื่อนถอยกลับมายังพื้นที่ปลอดภัย<br/>`,
			style:{'top':'19%','left':'17%','right':'unset','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_1',
			img: Imglist['control_1']
		},
		{
			content:`<span class='text--blue'>ร้านค้าและเงิน</span> เมื่อเรามีเงินครบตามราคาของไอเท็มที่เราได้เซตไว้
						ก่อนเริ่มเกมไอเท็มจะปรากฎ สามารถกดที่รูปไอเท็มที่ต้องการซื้อได้เลย 
						หรือถ้าหากต้องการซื้อไอเท็มชิ้นอื่น ให้กดรูปรถเข็นเพื่อไปยังร้านค้า`,
			style:{'top':'45%','left':'5%','right':'unset','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_2',
			img: Imglist['control_2']
		},
		{
			content:`<span class='text--blue'>เลือดของป้อม</span> ซึ่งก็คือพลังชีวิตของป้อมนั่นเอง ซึ่งเราสามารถ
						ดูพลังที่เหลือของป้อมนั้นๆ ได้จากแถบด้านบน ซึ่งป้อมแต่ละป้อม 
						มีพลังชีวิตต่างกัน ป้อมด้านในสุด ก็จะมีพลังชีวิตสูงสุด `,
			style:{'top':'unset','left':'29%','right':'unset','bottom':'24%','zIndex':'200',width:'5%'},
			id:'control_3',
			img: Imglist['control_3']
		},
		{
			content:`<span class='text--blue'>เลือดและมานา</span><br/>
						<span class='text--blue'>เลือด</span> หรือก็คือพลังชีวิต ของฮีโร่ตัวนั้นนั่นเอง<br/><br/>
						<span class='text--blue'>มานา</span> ซึ่งทุกครั้งที่ใช้สกิลจะต้องจ่ายมานา
						เป็นการแลกเปลี่ยน หากมานาหมดก็จะไม่สามารถใช้สกิลได้ ดังนั้น
						ถ้าเหลือมานาน้อย ก็ยังไม่ควรเข้าไปโจมตีศัตรู`,
			style:{'top':'34%','left':'unset','right':'46%','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_4',
			img: Imglist['control_4']
		},
		{
			content:`<span class='text--blue'>เวลาที่เล่น</span> เวลาที่ปรากฏคือระยะเวลาที่เราได้เข้ามาอยู่ในเกม <br/>
						<span class='text--blue'>จำนวนศัตรูที่สามารถฆ่าได้</span> แถบสีน้ำเงิน และแดงนั้นจะแสดงจำนวนศัตรูทั้งหมด
						ที่ทุกคนในแต่ละทีมสามารถฆ่าได้ (ฝั่งสีน้ำเงินคือฝั่งของเรา ฝั่งสีแดงคือฝั่งของศัตรู)<br/><br/>
						<span class='text--blue'>ความเร็วอินเตอร์เน็ต</span> บอกคุณภาพของสัญญาณอินเตอร์เน็ต <br/>
						<span class='text--blue'>แบตเตอรี่</span> ค่าแบตเตอรี่ที่เหลืออยู่ ของอุปกรณ์ที่เราใช้เล่น<br/>
						<span class='text--blue'>Kill</span> จำนวนของศัตรู ที่เราสามารถฆ่าได้<br/>
						<span class='text--blue'>Death</span> จำนวนครั้งที่เราตาย<br/>
						<span class='text--blue'>Assist</span> จำนวนครั้งที่เราช่วยเหลือเพื่อนในการฆ่าศัตรู<br/>
						`,
			style:{'top':'1%','left':'unset','right':'18%','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_5',
			img: Imglist['control_5']
		},
		{
			content:`<span class='text--blue'>สัญญาณสื่อสารกับทีม</span> เป็นสัญญาณที่ใช้สะดวก และรวดเร็ว ทั้งนี้เพื่อการสื่อสารกับเพื่อนๆ ในทีม
						ให้เป็นไปได้อย่างทันท่วงที ทั้งยังทำให้เพื่อนเข้าใจตรงกันอีกด้วย โดยจะมีสัญญาณ 4 อย่าง ดังนี้<br/><br/>
						- <span class='text--blue'>โจมตี</span> เป็นสัญญาณเพื่อบอกให้เพื่อนในทีมโจมตีศัตรู<br/>
						- <span class='text--blue'>ถอย</span> สัญญาณนี้ใช้เพื่อบอกให้เพื่อนในทีมถอยมายังที่ปลอดภัย อย่าพึ่งเข้าโจมตีศัตรู<br/>
						- <span class='text--blue'>รวมตัว</span> มักใช้เพื่อเรียกให้เพื่อนในทีมมารวมตัวที่ตำแหน่งเดียวกัน เพื่อเตรียมโจมตี<br/>
						- <span class='text--blue'>พิมพ์</span> สามารถเลือกพิมพ์เองก็ได้ หากสิ่งที่เราจะบอกเพื่อนในทีม อยู่นอกเหนือจากสัญญาณคำสั่งข้างต้น<br/>`,
			style:{'top':'18%','left':'unset','right':'1%','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_6',
			img: Imglist['control_6'],
			instyle:{maxWidth: '60%'},
		},
		{
			content:`<span class='text--blue'>ค่าสถานะ</span> เมื่อกดที่ลูกศรด้านขวามือ ก็จะทำให้มีค่าสถานะต่างๆ ของฮีโร่
						ของเรา ณ ตอนนั้นๆ ปรากฏขึ้นมา ดังรูป`,
			style:{'top':'44%','left':'unset','right':'1%','bottom':'unset','zIndex':'200',width:'5%'},
			id:'control_7',
			img: Imglist['control_7']
		},
		{
			content:`<span class='text--blue'>สกิล</span> พลังเฉพาะของฮีโร่ตัวนั้นๆ ซึ่งฮีโร่ส่วนใหญ่จะมี 3 สกิล<br/><br/>

						<span class='text--blue'>ปุ่มโจมตี</span><br/>
						- <span class='text--blue'>โจมตีธรรมดา</span> ปุ่มรูปกำปั้นคือการโจมตีอัตโนมัติ ธรรมดาๆ<br/>
						- <span class='text--blue'>เลือกโจมตีเฉพาะครีป</span> ปุ่มด้านล่างซ้ายของ ปุ่มโจมตีธรรมดา หากกด
						ฮีโร่จะทำการโจมตีเฉพาะครีป ถึงแม้จะมีศัตรู หรือป้อมอยู่ใกล้ๆ ก็ตาม<br/>
						- <span class='text--blue'>เลือกโจมตีเฉพาะป้อม</span> ปุ่มด้านบนขวาของ ปุ่มโจมตีธรรมดา หากกด
						ฮีโร่จะทำการโจมตีเฉพาะป้อม ถึงแม้จะมีศัตรู หรือครีปอยู่ใกล้ๆ ก็ตาม`,
			style:{'top':'unset','left':'unset','right':'17%','bottom':'27%','zIndex':'200',width:'5%'},
			id:'control_8',
			img: Imglist['control_8']
		},
		{
			content:`<span class='text--blue'>ปุ่มวาร์ป</span> มีหน้าที่พาฮีโร่ของเรากลับไปยังจุดที่เกิด โดยใช้ระยะเวลาในการวาร์ป
						ระยะหนึ่ง ใช้เมื่ออยู่ในสถานการณ์ เช่น เลือด หรือมานา ใกล้จะหมด 
						ซึ่งการวาร์ปที่ดี ควรไปแอบในพุ่มไม้ก่อนที่จะกดปุ่มวาร์ปกลับ เพื่อป้องกัน
						การถูกโจมตีระหว่างการใช้ เพราะในขณะที่เรากำลังวาร์ป จะไม่สามารถใช้สกิล 
						หรือเคลื่อนที่ได้ หากมีการเคลื่อนไหวการวาร์ปจะหยุดลงทันที`,
			style:{'top':'unset','left':'unset','right':'39%','bottom':'7%','zIndex':'200',width:'5%'},
			id:'control_9',
			img: Imglist['control_9']
		},
		{
			content:`<span class='text--blue'>ปุ่ม Challenger Spell</span> คือ สกิลพื้นฐาน ซึ่ง Challenger Spell 
						นั้นมีให้เลือกใช้งานทั้งหมด 9 Spell โดยในแต่ละเกมเราจะสามารถเลือก Spell ที่เหมาะสมไปใช้ได้ 1 Spell`,
			style:{'top':'unset','left':'unset','right':'31%','bottom':'7%','zIndex':'200',width:'5%'},
			id:'control_10',
			img: Imglist['control_10']
		},
	]

export default props=>{
	return(
		<div className={styles['wrapper']}>
			<div className={[styles['topcontent'],styles['control']].join(' ')}>
				 คำอธิบายสัญลักษณ์ต่าง ๆ ภายในเกม ว่าแต่ละอันมีหน้าที่อะไร และใช้งานอย่างไร
				<span className={styles['text--red']}> เพื่อทำให้การเล่นมีีีีประสิทธิภาพ</span>
				มากขึ้น
			</div>
			<div className={styles['container']}>
					<div className={styles['imgbgcontainer']}>
						<img src={Imglist['controlui_img']} alt=""/>
						<div className={styles['imgbgcontainer__cover']}>
							{
								btnlist.map((btn,key) => {
									return(
											<Popupbtn instyle={btn.style} img={Imglist['exclamation_icon']} imgstyle={{'maxWidth':'100%',cursor:'pointer',display:'block'}} id={btn.id} key={'homeuibtn' + key}>
													<div className={styles['control__wrap']}>
														<div className={styles['control__imgbox']}>
															<img src={btn.img} style={btn.instyle} alt=""/>
														</div>
														<div className={styles['control__content']}>
																<font dangerouslySetInnerHTML={{ __html: btn.content }} />
														</div>
													</div>
											</Popupbtn>
									)
								})
							}
						</div>
					</div>
					<div className={[styles['text--red'],styles['botsection__text']].join(' ')}>คลิกที่สัญลักษณ์ <img className={styles['exclameimg']} src={Imglist['exclamation_icon']} alt=""/> เพื่อดูคำอธิบายเพิ่มเติม </div>
			</div>
		</div>
	);
}	