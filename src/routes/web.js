import React, {Component,Fragment} from 'react';
import {Route} from 'react-router-dom';

import { GuideHome } from './../pages/GuideHome'
import { GuideHero } from './../pages/GuideHero'
import { GuideMap } from './../pages/GuideMap'
import { GuideItem } from './../pages/GuideItem'
import { GuideSummary } from './../pages/GuideSummary'
import { GuideAdvance } from './../pages/GuideAdvance'

export class Web extends Component{
	render(){
		return(
			<Fragment>
				<Route path="/" exact component={GuideHome}/>
				<Route path="/hero" exact component={GuideHero}/>
				<Route path="/map" exact component={GuideMap}/>
				<Route path="/item" exact component={GuideItem}/>
				<Route path="/summary" exact component={GuideSummary}/>
				<Route path="/advance" exact component={GuideAdvance}/>
			</Fragment>
		);
	}
}